

var path;

var myOgStuff = {
  drawnPaths: [],
  currentdrawPathIndex: 0,
  getCurrentPath: function() {
    return myOgStuff.drawnPaths[myOgStuff.currentdrawPathIndex]
  },
  intersectionCircles: [],
  intersections: [],
};



///*** Preparations ***///

var textItem = new PointText({
	content: 'Click and drag to draw a first loop.',
	point: new Point(20, 30),
	fillColor: 'black',
});



///*** Drawing ***///

function onMouseDown(event) {
  console.log(myOgStuff.drawnPaths);
	if (myOgStuff.drawnPaths.length === 0) {
    myOgStuff.drawnPaths = []
    console.log("hellooo")

    // Create a new path and set its stroke color to black:
    myOgStuff.currentdrawPathIndex = myOgStuff.drawnPaths.length;
    var newPath = new Path({
      segments: [event.point],
      strokeColor: 'black',
      // Select the path, so we can see its segment points:
      fullySelected: true
    });

    myOgStuff.drawnPaths.push(newPath)
	} else {
    // console.log("segments:", path.segments.length);
  }
}

function onMouseDrag(event) {
  var currentPath = myOgStuff.getCurrentPath();
  if (currentPath.closed === false) {
    currentPath.add(event.point);
  } else {
    currentPath.position += event.delta;
  }

	// Update the content of the text item to show how many
	// segments it has:
	// textItem.content = 'Segment count: ' + path.segments.length;
}

// When the mouse is released, we simplify the path:
function onMouseUp(event) {
	// var segmentCount = path.segments.length;

  var currentPath = myOgStuff.getCurrentPath();
  if (currentPath.closed === false) {
    currentPath.closed = true;
    currentPath.smooth({
      type: "geometric",
      from: currentPath.segments.length - 1,
      to: 1
    });

    // When the mouse is released, simplify it:
    currentPath.simplify(25);
  }


	// // Select the path, so we can see its segments:
	// path.fullySelected = true;

	// var newSegmentCount = path.segments.length;
	// var difference = segmentCount - newSegmentCount;
	// var percentage = 100 - Math.round(newSegmentCount / segmentCount * 100);
	// textItem.content = difference + ' of the ' + segmentCount + ' segments were removed. Saving ' + percentage + '%';
	textItem.content = 'Drag to move curve.\nPress keyboard button and click (and drag) for the following:\nN: new loop\n';

  showSelfIntersections(currentPath);
}


function onKeyDown(event) {
	if(event.key == 'n') {
    currentPath = myOgStuff.getCurrentPath();
    currentPath.remove();
    myOgStuff.drawnPaths = [];
    for (c in myOgStuff.intersectionCircles) {
      c.remove();
    }
    myOgStuff.intersections = [];
    myOgStuff.intersectionCircles = [];
	}
}

function showSelfIntersections(path) {
  var intersections = path.getIntersections(path);
  for (var i = 0; i < intersections.length; i++) {
    var intCircle = new Path.Circle({
      center: intersections[i].point,
      radius: 5,
      fillColor: '#009dec'
    }).removeOnDrag();
    myOgStuff.intersectionCircles.push(intCircle);
    myOgStuff.intersections = intersections;
  }
}