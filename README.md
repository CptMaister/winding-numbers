# winding-numbers

Simple JavaScript application that calculates the winding numbers of all the components of a closed curve using [Paper.js](http://paperjs.org/).

Check the current version live here: https://cptmaister.gitlab.io/winding-numbers/

## TODOs

### Start
1. ✅ restart working on project, make sense of the [old code](./public/archive/)
1. ✅ implement old code with pure JavaScript and injected PaperScript
1. ✅ read through the [Paper.js Tutorials](http://paperjs.org/tutorials/) and save [useful stuff](./paper-js-docs-useful-stuff/), maybe test some of it already
1. ✅ write simple test suite with some example curves to test against
1. ✅ write this TODO list || [2a4d497](https://gitlab.com/CptMaister/winding-numbers/-/commit/950a9fbc76f0415f5cac14937fe70f080dae29ad)
1. ✅ write wrapper class that contains a curve and all its stuff (intersections, parts, connected components, calculated results, even the canvas, ...)


### Connected Components
1. ✅ split path into parts
1. ✅ combine parts to connected components (or rather their boundaries)
1. ✅ visualize all connected components with different colors
    - ✅ take special care with the unbounded component


### Path to $J^+$
1. ✅ calculate the winding numbers of the connected components
1. ✅ calculate the double point indices
1. ✅ calculate the $J^+$-invariant


### UI immersion analysis info
1. ✅ show drawn immersion's stuff, like the calculated J+ value
1. show predictions on generated curves
1. show indications whether the calculated values are consistent, i.e. formulas between the values like number of immersion parts double the number of crossings (except for circle embedding), or number of connected components is two more than the number of crossings


### Manipulate Immersion
1. ✅ delete segments
1. move segments
1. add segments


### Automate drawing special immersions
#### Bifurcations
1. ✅ create bifurcation concept
1. ✅ bifurcate a curve
1. implement calculation and display of bifurcation formulas
1. ...

#### $T_{k,l}$-torus type orbits
1. ...

#### Levi-Civita regularization
1. let user set the origin
1. calculate the $\mathcal{J}_1$-invariant
1. transform curve to get the preimage under the Levi-Civita regularization map (complex squaring map)

#### Birkhoff regularization
1. let user set two special points
1. transform curve to get the preimage under the Birkhoff regularization map (complex squaring map)


### Other Stuff
1. ✅ add UI elements for showing results which are currently only in the console
1. ✅ add UI buttons and keyboard-shortcuts for implemented features
1. ✅ multiple curves drawable and easily selectable


## Using npm, without having to deploy npm:
Luckily not needed after all, but for future reference, use the second of the three methods described in [this YouTube-video](https://youtu.be/qTnCSrp7Qmg?t=216) (with vite).
