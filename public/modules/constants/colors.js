
const COLOR_PALETTE_DEBUGGING = [
  '#FFBE98',
  '#F05A7E',
  '#125B9A',
  '#0B8494',
  '#C7253E',
  '#7A1CAC',
  '#AD49E1',
  '#3795BD',
  '#5B99C2',
  '#1A4870',
  '#96CEB4',
  '#FF8343',
  '#179BAE',
]
const COLOR_PALETTE_AREA = [
  '#ff0000',
  '#ff8930',
  '#ffff00',
  '#00ff00',
  '#00ffff',
  '#0000ff',
  '#660099',
  '#ff00ff',
]

export { COLOR_PALETTE_DEBUGGING, COLOR_PALETTE_AREA };