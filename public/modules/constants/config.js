
const CANVAS_ID = 'myCanvas';

const SHORTCUT_KEYS = {
  RESET: 'r',
  SELECT: 's',
  RESIZE_BIGGER: '+', // TODO ALEX: https://developer.mozilla.org/en-US/docs/Web/API/Element/wheel_event
  RESIZE_SMALLER: '-',
  TEST_STUFF: "t", // TODO END: remove
  BIFURCATE: "b", // TODO ALEX: move this to ui
}

const CTRL_SHORTCUT_KEYS = {
  // TODO ALEX END
}

export { CANVAS_ID, SHORTCUT_KEYS, CTRL_SHORTCUT_KEYS };
