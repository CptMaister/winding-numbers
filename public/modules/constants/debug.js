
///*** Debugging Stuff ***///

const DEBUGGING_TESTS_RUN_ALL = false; // TODO END set this to false
const DEBUGGING_VISUALIZE_PATH_PARTS = false; // set this to true to see the newly created parts. Make sure to draw a small immersion, since the shift between the parts might be small.
const DEBUGGING_VISUALIZE_PATH_PARTS_SHIFT = 80;

const DEBUGGING_TEST_CURVES_DESTROY_EVERYTHING = false;

export {
  DEBUGGING_TESTS_RUN_ALL,
  DEBUGGING_VISUALIZE_PATH_PARTS,
  DEBUGGING_VISUALIZE_PATH_PARTS_SHIFT,
  DEBUGGING_TEST_CURVES_DESTROY_EVERYTHING,
};
