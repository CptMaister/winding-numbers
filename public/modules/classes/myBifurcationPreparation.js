import MyImmersion from "./myImmersion.js";

import {
  last,
  range,
  sortLengthsAscending,
  sortNumbersAscending,
} from "../helpers/basic.js";
import {
  getMiddlePointOfPath,
  joinFragmentaryPathPartsToClosedPath,
  createPartsClonesAtOffset,
  addGapToPathAtPoint,
} from "../helpers/paper.js"


const DEFAULT_BIFURCATION_NUMBER = 2;
const BIF_PREVIEW_STROKE_WIDTH = 1;
const BIF_PREVIEW_STROKE_COLOR = '#00ff00';
const BIF_PREVIEW_STROKE_OPACITY = 0.8;

const MAX_OFFSET_LOOP_LIMIT_SOFT = 5;
const MAX_OFFSET_LOOP_LIMIT_HARD = 90;
const MAX_OFFSET_FIRST_CANDIDATE_FACTOR = (3/10); // TODO ALEX 123 start smaller and then before the binary search add this size on and on, until it does not work (or goes above the MAX_OFFSET_MAX_ALLOWED_FACTOR).
const MAX_OFFSET_MAX_ALLOWED_FACTOR = (4/10);

class MyBifurcationPreparation {
  /**
   * @type {MyImmersion}
   */
  #parentImmersion;
  /**
   * @type {paper.Path}
   */
  #parentImmersionPathClone;
  /**
   * @type {paper.Path[]}
   */
  #parentImmersionPartsPathClones;

  /**
   * Whether there is a visible preview.
   * @type {boolean}
   */
  #visiblePreview;
  /**
   * Where the preview of finishedBifurcationCandidate is centered.
   * @type {path.Point}
   */
  #previewCenter;

  /**
   * Is true if #maxOffset could not be determined. Only used there when this was added, but might be used elsewhere as well.
   * @type {boolean}
   */
  #failedInitialization = false;
  /**
   * The "tube radius", i.e. the maximum distance a pre-bifurcation clone should be away from the original immersion.
   * @type {number}
   */
  #maxOffset;
  /**
   * Take all connected components of the original immersion, take their bounding rectangles' length, of these the shortest length.
   * Created during the search for the maxOffset, and useful later.
   * @type {number}
   */
  #smallestBoundsLengthOfCCs
  /**
   * The location, on the parentImmersionPathClone, where the neat connections should be. By default calculated as the middle of the longest immersion part.
   * @type {paper.CurveLocation}
   */
  #neatConnectionsPointCandidate;

  /**
   * How many clones of the original immersion shall be created and then connected.
   * @type {integer} positive, >= 1
   */
  #bifurcationNumber;
  /**
   * Ordered from 0 to (bifurcationNumber-1), starting at negative maxOffset and up to positive maxOffset.
   * @type {paper.Path[]}
   */
  #pathClones;
  /**
   * The finished bifurcation, at first shown to the user as preview, but once accepted, will be returned for further processing, like creating a new MyImmersion object from it.
   * @type {paper.Path}
   */
  #finishedBifurcationCandidate;

  /**
   * @type {paper.Group}
   */
  #visibleElementsGroup;



  /**
   * @param {MyImmersion} parentImmersion
   * @param {boolean} visiblePreview
   * @param {paper.Point} previewCenter
   */
  constructor(parentImmersion, visiblePreview=true, previewCenter=undefined) {
    this.#parentImmersion = parentImmersion;
    this.#visiblePreview = visiblePreview;
    this.#previewCenter = previewCenter ?? parentImmersion.center;

    this.#checkPrerequisites();
    this.#initializePreparations();
    this.#calculateMaxOffset();
    this.#findNeatConnectionsPointCandidate();

    this.#createPreviewBifurcation(DEFAULT_BIFURCATION_NUMBER);
  }

  #checkPrerequisites() {
    if (this.#parentImmersion.closed === false) {
      throw new Error('MyBifurcationPreparation initializePreparations: immersion\'s path is not closed.');
    }
    if (!this.#parentImmersion.validImmersion) {
      throw new Error('MyBifurcationPreparation initializePreparations: immersion is not a valid immersion (e.g. has tangential crossings).');
    }
    if (this.#parentImmersion.immersionParts === undefined || this.#parentImmersion.immersionParts.length < 1) {
      throw new Error('MyBifurcationPreparation initializePreparations: immersion does not have immersionParts yet.');
    }
  }

  #initializePreparations() {
    this.#parentImmersionPathClone = this.#parentImmersion.pathClone;
    this.#parentImmersionPathClone.visible = false;

    this.#parentImmersionPartsPathClones = this.#parentImmersion.immersionParts
      .map(immersionPart => {
        const partClone = immersionPart.path.clone();
        partClone.visible = false;
        return partClone;
      });

    this.#visibleElementsGroup = new paper.Group();
    this.#visibleElementsGroup.addChild(this.#parentImmersionPathClone);
    this.#visibleElementsGroup.addChildren(this.#parentImmersionPartsPathClones);

    this.#parentImmersion.addVisibleElement(this.#visibleElementsGroup);
  }

  #calculateMaxOffset() {
    const [firstOffsetCandidate, totalAllowedMax] = this.#findFirstMaxOffsetCandidate();

    // binary search values
    let currentOffsetCandidate = firstOffsetCandidate;
    let nextSizeChange = currentOffsetCandidate;
    let nextChangePositive = true;

    // loop management
    let currentBestCandidate = undefined;
    let jumpedBackLoopIteration = undefined;
    let loopCounter = 0;

    // here we collect created path objects
    let leftMaxOffsetParts = [];
    let rightMaxOffsetParts = [];
    let leftMaxOffsetPath = undefined;
    let rightMaxOffsetPath = undefined;

    while (true) {

      // loop counter and limit stuff
      if (loopCounter > MAX_OFFSET_LOOP_LIMIT_SOFT && currentBestCandidate !== undefined) {
        break;
      }
      if (loopCounter > MAX_OFFSET_LOOP_LIMIT_HARD) {
        break
      }
      loopCounter++;


      // adjust binary search values
      if (jumpedBackLoopIteration !== undefined) {
        nextSizeChange = nextSizeChange / 2;
        nextChangePositive = !jumpedBackLoopIteration;

        currentOffsetCandidate += ((nextChangePositive ? 1 : (-1)) * nextSizeChange);

        // console.log({nextSizeChange, nextChangePositive, currentOffsetCandidate, currentBestCandidate})
      }
      jumpedBackLoopIteration = false;


      // clean up created path objects
      // leftMaxOffsetParts.forEach(part => part.remove());
      // rightMaxOffsetParts.forEach(part => part.remove());
      leftMaxOffsetParts = [];
      rightMaxOffsetParts = [];
      if (leftMaxOffsetPath !== undefined) {
        leftMaxOffsetPath.remove();
        leftMaxOffsetPath = undefined;
      }
      if (rightMaxOffsetPath !== undefined) {
        rightMaxOffsetPath.remove();
        rightMaxOffsetPath = undefined;
      }


      // check 1: during parts cloning: no part clone can have intersections with itself (except at the ends)
      const [leftSuccess, leftOffsetParts] = createPartsClonesAtOffset(
        this.#parentImmersionPartsPathClones, -currentOffsetCandidate);
      let success = leftSuccess;

      if (leftSuccess) {
        leftMaxOffsetParts = leftOffsetParts;

        const [rightSuccess, rightOffsetParts] = createPartsClonesAtOffset(
          this.#parentImmersionPartsPathClones, currentOffsetCandidate);
        if (rightSuccess) {
          rightMaxOffsetParts = rightOffsetParts;
        }

        success = rightSuccess;
      }

      if (!success) {
        jumpedBackLoopIteration = true;
        continue;
      }
      // console.log("1st check passed");


      // check 2: single created LR-offsets: has same amount of self-intersections as original immersion
      const expectedIntersections = this.#parentImmersion.selfIntersections.length;

      const trailingPathPartCutoff = Math.min((currentBestCandidate ?? currentOffsetCandidate) / 13, this.#smallestBoundsLengthOfCCs / 50); // kinda arbitrary choice of sensible magic numbers
      leftMaxOffsetPath = (leftMaxOffsetParts.length === 1 ? leftMaxOffsetParts[0] : joinFragmentaryPathPartsToClosedPath(leftMaxOffsetParts, trailingPathPartCutoff));
      rightMaxOffsetPath = (rightMaxOffsetParts.length === 1 ? rightMaxOffsetParts[0] : joinFragmentaryPathPartsToClosedPath(rightMaxOffsetParts, trailingPathPartCutoff));

      const leftIntersections = leftMaxOffsetPath.getCrossings(leftMaxOffsetPath);
      const rightIntersections = rightMaxOffsetPath.getCrossings(rightMaxOffsetPath);

      if (
        leftIntersections.length !== expectedIntersections
        || rightIntersections.length !== expectedIntersections
      ) {
        jumpedBackLoopIteration = true;
        continue;
      }
      // console.log("2nd check passed");


      // check 3: L has 2n intersections with R, if original immersion has n intersections.
      if (
        leftMaxOffsetPath.getIntersections(rightMaxOffsetPath).length !== 2 * expectedIntersections
      ) {
        jumpedBackLoopIteration = true;
        continue;
      }
      // console.log("3rd check passed");


      // check 4: same for original with L and original with R
      if (
        this.#parentImmersionPathClone.getIntersections(rightMaxOffsetPath).length !== 2 * expectedIntersections
        || this.#parentImmersionPathClone.getIntersections(leftMaxOffsetPath).length !== 2 * expectedIntersections
      ) {
        jumpedBackLoopIteration = true;
        continue;
      }
      // console.log("4th check passed");


      // finished all checks, so the currentOffsetCandidate should be valid
      if (currentBestCandidate === undefined || currentOffsetCandidate > currentBestCandidate) {
        // console.log({p: "finish update", currentBestCandidate});
        currentBestCandidate = currentOffsetCandidate;
      }

      if (currentBestCandidate > totalAllowedMax) {
        break;
      }
    }

    if (currentBestCandidate === undefined) {
      this.#failedInitialization = true;
      console.warn("MyBifurcationPreparation calculateMaxOffset failed, currentBestCandidate === undefined is true");
      return;
    }

    this.#maxOffset = Math.min(totalAllowedMax, currentBestCandidate);
  }

  #findFirstMaxOffsetCandidate() {
    const smallerBoundsLengthsOfConnectedComponents = this.
      #parentImmersion
      .connectedComponents
      .filter(cc => !cc.isUnboundedComponent)
      .map(cc => {
        const boundingRectangle = cc.boundaryPath.bounds;
        return Math.min(boundingRectangle.width, boundingRectangle.height);
      })
      .sort(sortNumbersAscending);

    const smallestBoundsLengthOfCCs = smallerBoundsLengthsOfConnectedComponents[0];
    this.#smallestBoundsLengthOfCCs = smallestBoundsLengthOfCCs;

    const firstOffsetCandidate = smallestBoundsLengthOfCCs * MAX_OFFSET_FIRST_CANDIDATE_FACTOR;
    const totalAllowedMax = smallestBoundsLengthOfCCs * MAX_OFFSET_MAX_ALLOWED_FACTOR;

    if (totalAllowedMax >= firstOffsetCandidate * 2) {
      throw new Error("the binary search loop could run infinitely if totalAllowedMax is at least twice of firstOffsetCandidate");
    }

    return [firstOffsetCandidate, totalAllowedMax];
  }

  #findNeatConnectionsPointCandidate() {
    const sortedPathParts = this.#parentImmersionPartsPathClones
      .map(part => ({ part, length: part.length }))
      .sort(sortLengthsAscending);
    const longestPathPart = last(sortedPathParts).part;

    const middleOfLongestPathPart = getMiddlePointOfPath(longestPathPart);

    this.#neatConnectionsPointCandidate = this.#parentImmersionPathClone.getLocationOf(middleOfLongestPathPart);
  }

  destroySelf() {
    this.#visibleElementsGroup.remove();
  }


  // custom getters, setters

  /**
   * @param {integer} bifNumber >= 1
   */
  set bifurcationNumber(bifNumber) {
    if (MyBifurcationPreparation.isValidBifurcationNumber(bifNumber)) {
      this.#bifurcationNumber = bifNumber;
      this.#createPreviewBifurcation();
    } else {
      console.warn('set bifurcationNumber: invalid bifurcationNumber');
    }
  }
  /**
   * @returns {integer} bifNumber >= 1
   */
  get bifurcationNumber() {
    return this.#bifurcationNumber;
  }

  /**
   * @returns {paper.Path}
   */
  get finishedBifurcation() {
    return this.#finishedBifurcationCandidate;
  }

  /**
   * @param {integer} bifurcationNumber positive, >= 1
   */
  #createPreviewBifurcation(
    bifurcationNumber=this.#bifurcationNumber
  ) {
    if (this.#finishedBifurcationCandidate !== undefined) {
      this.#finishedBifurcationCandidate.remove();
    }

    if (bifurcationNumber === 1) {
      this.#finishedBifurcationCandidate = this.#parentImmersionPathClone.clone();
    } else {
      this.#finishedBifurcationCandidate = this.#createBifurcation(bifurcationNumber);
    }

    this.#visibleElementsGroup.addChild(this.#finishedBifurcationCandidate);

    this.#visualizePreviewBifurcation();
  }

  /**
   * @param {integer} bifurcationNumber positive, >= 2
   * @returns {paper.Path}
   */
  #createBifurcation(
    bifurcationNumber=this.#bifurcationNumber,
    maxOffset=this.#maxOffset
  ) {
    const pathClones = this.#createAllBifurcationPathClones(bifurcationNumber, maxOffset);

    const gapLength = maxOffset * 2;
    const gapLengthRelative = gapLength / this.#parentImmersionPathClone.length
    const shortestPathCloneLength = pathClones
      .sort(sortLengthsAscending)[0].length;
    const gapLengthMin = gapLengthRelative * shortestPathCloneLength;

    const pathsWithGaps = pathClones.map(pathClone => {
      return addGapToPathAtPoint(gapLengthMin, pathClone, this.#neatConnectionsPointCandidate.point)
    });

    return joinFragmentaryPathPartsToClosedPath(pathsWithGaps);
  }

  /**
   * Returned pathClones are sorted naturally, i.e. starting at one side of the offset, then going one by one to the other side of the offset.
   * @param {integer} bifurcationNumber positive, >= 2
   * @param {number} maxOffset
   * @returns {paper.Path[]}
   */
  #createAllBifurcationPathClones(
    bifurcationNumber=this.#bifurcationNumber,
    maxOffset=this.#maxOffset
  ) {
    if (!MyBifurcationPreparation.isValidBifurcationNumber(bifurcationNumber)) {
      console.warn('set bifurcationNumber: invalid bifurcationNumber');
      return;
    }

    const hasMiddleClone = ((bifurcationNumber % 2) !== 0);
    const middleCloneIndex = (
      hasMiddleClone ?
      ((bifurcationNumber - 1) / 2) : // note that we need to do "-1" instead of "+1", because index starts at 0, of course
      undefined
    );

    const clonedPaths = range(0, bifurcationNumber)
      .map(index => {
        let pathClone;

        // TODO ALEX 123 456: add reproducible tests with the failing method of taking segments out (instead of length, see commit from 2024-10-20 and -21). To do this, find a good way to return segments (with their handles) to console and use them in the tests. See if the errors occur reliably, then write tests with expection the errors, then the ones with expecting to have fixed the errors. And then in general add some bifurcation testing!
        if (index === middleCloneIndex) {
          // for the middle clone, we just take the original immersion instead of creating part clones and stitching them together
          pathClone = this.#parentImmersionPathClone.clone();
        } else {
          const offsetDistance = (2 * (index / (bifurcationNumber-1)) - 1) * maxOffset;
          pathClone = this.#createPathCloneAtOffset(offsetDistance);
        }

        return {
          index,
          pathClone
        }
      })
      .sort((p1, p2) => p1.index - p2.index)
      .map(indexedPath => indexedPath.pathClone);

    return clonedPaths;
  }

  /**
   * Use external Library paperjs-offset to create clones of the path parts, which are then joined together and returned.
   * @param {number} offset
   * @returns {paper.Path}
   */
  #createPathCloneAtOffset(offset) {
    const partOffsetClones = this.#parentImmersionPartsPathClones.map(partPath => {
      const partClone = PaperOffset.offset(partPath, offset, {insert: true});

      return partClone;
    });

    const trailingPathPartCutoff = (
      this.#maxOffset !== undefined ?
      Math.min(this.#maxOffset / 13, this.#smallestBoundsLengthOfCCs / 50) :
      partClone.length / 500
    ); // kinda arbitrary choice of sensible magic numbers
    return joinFragmentaryPathPartsToClosedPath(partOffsetClones, trailingPathPartCutoff);
  }

  #visualizePreviewBifurcation() {
    this.#finishedBifurcationCandidate.visible = true;
    this.#finishedBifurcationCandidate.strokeColor = BIF_PREVIEW_STROKE_COLOR;
    this.#finishedBifurcationCandidate.strokeWidth = BIF_PREVIEW_STROKE_WIDTH;
    this.#finishedBifurcationCandidate.opacity = BIF_PREVIEW_STROKE_OPACITY;
  }


  // helpers

  /**
   * @param {integer} bifurcationNumber
   * @returns {boolean}
   */
  static isValidBifurcationNumber(bifurcationNumber) {
    return Number.isInteger(bifurcationNumber) && bifurcationNumber >= 1;
  }
}


export default MyBifurcationPreparation;
