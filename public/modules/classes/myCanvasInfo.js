

const CANVAS_TEXT_COLOR = 'black';
// const CANVAS_TEXT_INITIAL = 'Click and drag to draw a first immersion.';
const CANVAS_TEXT_INITIAL = '';
// const CANVAS_TEXT_SHORTCUTS = `Drag to move immersion.
//  ${SHORTCUT_KEY_RESET.toUpperCase()}: reset canvas
//  ${SHORTCUT_KEY_RESIZE_BIGGER.toUpperCase()} and ${SHORTCUT_KEY_RESIZE_SMALLER.toUpperCase()}: resize immersion
// `;
// ${SHORTCUT_KEY_SELECT.toUpperCase()}: toggle path select

class MyCanvasInfo {
  textItem; // paper.PointText

  constructor() {
    this.textItem = new paper.PointText({
      content: CANVAS_TEXT_INITIAL,
      point: new paper.Point(20, 30),
      fillColor: CANVAS_TEXT_COLOR,
    });
  }

  /**
   * @param {string} newText
   */
  updateCanvasText(newText) {
    this.textItem.content = newText;
  }
}

export default MyCanvasInfo;

export { CANVAS_TEXT_COLOR };
