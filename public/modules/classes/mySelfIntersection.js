import MyImmersion from "./myImmersion.js";
import MyImmersionPart from "./myImmersionPart.js";
import MyConnectedComponent from "./myConnectedComponent.js";


class MySelfIntersection {
  /**
   * @type {MyImmersion}
   */
  parentImmersion; // MyImmersion
  curveLocations; // paper.CurveLocation[], array of 2 paper.CurveLocation, one for each of the two intersecting curve parts <- see http://paperjs.org/reference/curvelocation/

  // references to mathematical companions
  /**
   * @type {MyImmersionPart[]}
   */
  immersionParts; // array of MyImmersionPart, should be exactly 4
  /**
   * @type {MyConnectedComponent[]}
   */
  connectedComponents; // array of MyConnectedComponent, should be exactly 4, with one maybe counted double

  // calculated stuff
  #doublePointIndex; // int

  /**
   * @param {MyImmersion} parentImm
   * @param {paper.CurveLocation} curveLocation
   */
  constructor(parentImm, curveLocation) {
    this.parentImmersion = parentImm;
    this.curveLocations = [curveLocation, curveLocation.intersection];
  }

  destroySelf() {
    // nothing to do here
  }

  get point() {
    return this.curveLocations[0].point;
  }

  // get offsets() {
  //   return this.curveLocations.map(loc => loc.offset);
  // }

  // get tangents() {
  //   return this.curveLocations.map(loc => loc.tangent);
  // }

  // get normals() {
  //   return this.curveLocations.map(loc => loc.normal);
  // }

  // get curvatures() {
  //   return this.curveLocations.map(loc => loc.curvature);
  // }

  /**
   * @returns {integer}
   */
  get doublePointIndex() {
    if (this.#doublePointIndex === undefined) {
      this.#doublePointIndex = this.#calculateDoublePointIndex();
    }
    return this.#doublePointIndex;
  }

  /**
   * @param {MyImmersionPart} immersionPart 
   */
  addImmersionPart(immersionPart) {
    if (this.immersionParts === undefined) {
      this.immersionParts = [];
    }
    this.immersionParts.push(immersionPart);
  }
  /**
   * @param {MyConnectedComponent} connectedComponent 
   */
  addConnectedComponent(connectedComponent) {
    if (this.connectedComponents === undefined) {
      this.connectedComponents = [];
    }
    this.connectedComponents.push(connectedComponent);
    if (this.connectedComponents.length > 4) {
      console.error('MySelfIntersection.connectedComponents.length > 4');
    }
  }

  #calculateDoublePointIndex() {
    // calculate the arithmetic mean of the 4 surrounding connected components' winding number (maybe counted double). This is equal to the middle value of the 3 winding number values appearing around a double point, since the middle one is always the one appearing twice and the 3 values each differ by 1.
    if (this.connectedComponents.length === 4) {
      const windingNumbers = this.connectedComponents
        .map(cc => cc.windingNumber)
        .filter(wn => wn !== undefined);
      if (windingNumbers.length === 4) {
        const dpIndex = windingNumbers.reduce((a, b) => a + b, 0) / 4;
        return dpIndex;
      } else {
        console.warn(
          'MySelfIntersection calculateDoublePointIndex: not all windingNumbers available, need 4.',
          windingNumbers
        );
      }
    } else {
      console.warn(
        'MySelfIntersection calculateDoublePointIndex: not enough connected components',
        this.connectedComponents
      );
    }
    return undefined;
  }
}

export default MySelfIntersection;
