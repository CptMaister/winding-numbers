import MyImmersion from "./myImmersion.js";
import MyImmersionPart from "./myImmersionPart.js";
import MySelfIntersection from "./mySelfIntersection.js";
import MyConnectedComponent from "./myConnectedComponent.js";

import { Sides, isSide, flipSide } from "../enums/sides.js";


class MyOrientedCCImmersionPart {
  /**
   * @type {MyImmersionPart}
   */
  #originalImmersionPart; // MyImmersionPart
  reverseOrientation; // boolean
  /**
   * @type {Sides}
   */
  connectedComponentSide; // Sides.LEFT or Sides.RIGHT, as viewed from the oriented part's perspective, not from the original part

  adjustedPartPath; // paper.Path

  /**
   * @param {MyImmersionPart} immersionPart
   * @param {boolean} reversed
   * @param {Sides} side
   */
  constructor(immersionPart, reversed, side) {
    this.#originalImmersionPart = immersionPart;
    this.reverseOrientation = reversed;
    this.connectedComponentSide = side;

    if (!isSide(side)) {
      throw new Error('constructor MyOrientedCCImmersionPart with invalid side');
    }

    this.#additionalInitialization();
  }

  /**
   * Does not destroy the underlying immersionPart.
   */
  destroySelf() {
    // nothing to do here
  }

  #additionalInitialization() {
    this.#initializeAdjustedPartPath();
  }

  #initializeAdjustedPartPath() {
    const clonedPath = this.#originalImmersionPart.path.clone();
    if (this.reverseOrientation) {
      clonedPath.reverse()
    }
    this.adjustedPartPath = clonedPath;
    this.parentImmersion.addVisibleElement(this.adjustedPartPath);
    this.adjustedPartPath.visible = false;
  }

  /**
   * @returns {MyImmersion}
   */
  get parentImmersion() {
    return this.#originalImmersionPart.parentImmersion;
  }

  /**
   * @returns {Sides}
   */
  get realComponentSide() {
    return flipSide(this.connectedComponentSide, this.reverseOrientation);
  }

  /**
   * @returns {MyImmersionPart}
   */
  get originalImmersionPart() {
    return this.#originalImmersionPart
  }

  /**
   * @param {MyConnectedComponent} component
   */
  set connectedComponent(component) {
    if (this.realComponentSide === Sides.LEFT) {
      this.#originalImmersionPart.leftConnectedComponent = component;
    } else {
      this.#originalImmersionPart.rightConnectedComponent = component;
    }
  }

  get connectedComponent() {
    return this.#originalImmersionPart.getConnectedComponent(this.realComponentSide);
  }

  /**
   * @returns {MySelfIntersection}
   */
  get startPoint() {
    return this.#originalImmersionPart.startPoint;
  }
  /**
   * @returns {MySelfIntersection}
   */
  get endPoint() {
    return this.#originalImmersionPart.endPoint;
  }
  /**
   * @returns {MySelfIntersection}
   */
  get adjustedStartPoint() {
    return this.reverseOrientation ?
      this.endPoint :
      this.startPoint;
  }
  /**
   * @returns {MySelfIntersection}
   */
  get adjustedEndPoint() {
    return this.reverseOrientation ?
      this.startPoint :
      this.endPoint;
  }

  /**
   * @returns {paper.Point}
   */
  get startTangent() {
    return this.#originalImmersionPart.startTangent;
  }
  /**
   * @returns {paper.Point}
   */
  get endTangent() {
    return this.#originalImmersionPart.endTangent;
  }
  /**
   * @returns {paper.Point}
   */
  get adjustedStartTangent() {
    return this.reverseOrientation ?
      this.endTangent.multiply(-1) :
      this.startTangent;
  }
  /**
   * @returns {paper.Point}
   */
  get adjustedEndTangent() {
    return this.reverseOrientation ?
      this.startTangent.multiply(-1) :
      this.endTangent;
  }
}

export default MyOrientedCCImmersionPart;
