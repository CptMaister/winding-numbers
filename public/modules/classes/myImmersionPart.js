import MyImmersion from "./myImmersion.js";
import MySelfIntersection from "./mySelfIntersection.js";
import MyConnectedComponent from "./myConnectedComponent.js";

import { Sides } from "../enums/sides.js";

import {
  getMiddlePointOfPath,
} from "../helpers/paper.js"


class MyImmersionPart {
  /**
   * @type {MyImmersion}
   */
  parentImmersion; // MyImmersion
  path; // paper.Path, not necessarily closed

  /**
   * @type {MySelfIntersection}
   */
  startPoint; // MySelfIntersection
  /**
   * @type {MySelfIntersection}
   */
  endPoint; // MySelfIntersection

  debuggingName; // String

  // references to mathematical companions
  /**
   * @type {MyConnectedComponent}
   */
  leftConnectedComponent; // MyConnectedComponent
  /**
   * @type {MyConnectedComponent}
   */
  rightConnectedComponent; // MyConnectedComponent

  /**
   * @param {MyImmersion} parentImm
   * @param {paper.Path} part
   * @param {MySelfIntersection} start
   * @param {MySelfIntersection} end
   */
  constructor(parentImm, part, start, end, debuggingName) {
    this.parentImmersion = parentImm;
    this.path = part;
    this.parentImmersion.addVisibleElement(this.path);

    this.startPoint = start;
    this.endPoint = end;

    this.debuggingName = debuggingName;

    this.#additionalInitialization();
  }

  destroySelf() {
    // nothing to do here
  }

  /**
   * @returns {paper.Point}
   */
  get startTangent() {
    return this.path.getTangentAt(0);
  }
  /**
   * @returns {paper.Point}
   */
  get endTangent() {
    return this.path.getTangentAt(this.path.length);
  }

  get middlePoint() {
    return getMiddlePointOfPath(this.path);
  }

  #additionalInitialization() {
    this.#setBoundaryPointsImmersionPart();
  }

  #setBoundaryPointsImmersionPart() {
    if (this.startPoint !== null) {
      this.startPoint.addImmersionPart(this);
    }
    if (this.endPoint !== null) {
      this.endPoint.addImmersionPart(this);
    }
  }

  /**
   * @param {Sides} side
   * @returns {MyConnectedComponent}
   */
  getConnectedComponent(side) {
    if (side === Sides.LEFT) {
      return this.leftConnectedComponent;
    } else {
      return this.rightConnectedComponent;
    }
  }
}

export default MyImmersionPart;
