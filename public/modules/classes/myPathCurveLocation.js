import MySelfIntersection from "./mySelfIntersection.js";


class MyPathCurveLocation {
  curveLocation; // paper.CurveLocation <- see http://paperjs.org/reference/curvelocation/#segment
  /**
   * @type {MySelfIntersection}
   */
  locationOriginObject; // MySelfIntersection, at least usually, but can be anything that supplied the location information

  /**
   * @param {paper.CurveLocation} curveLocation
   * @param {any} locationOriginObject
   */
  constructor(curveLocation, locationOriginObject) {
    this.curveLocation = curveLocation;
    this.locationOriginObject = locationOriginObject;
  }

  get offset() {
    return this.curveLocation.offset;
  }
}

export default MyPathCurveLocation;
