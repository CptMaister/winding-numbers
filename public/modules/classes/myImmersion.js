import MyDrawingView from "./myDrawingView.js";
import MyImmersionPart from "./myImmersionPart.js";
import MySelfIntersection from "./mySelfIntersection.js";
import MyConnectedComponent from "./myConnectedComponent.js";
import MySettings from "./mySettings.js";
import MyBifurcationPreparation from "./myBifurcationPreparation.js";

import { VisualsImmersion } from "../enums/visuals.js";
import { FontFamily, FontWeight } from "../enums/font.js";

import { COLOR_PALETTE_AREA } from "../constants/colors.js";

import {
  properMod,
  rgbToHex,
  componentToHex,
} from "../helpers/basic.js";
import {
  visualizePointWithCircle,
  paperItemVisibility,
  getInnerPointInPathArea,
} from "../helpers/paper.js"
import {
  createPartsFromPathIntersections,
  createConnectedComponentsFromPathParts,
} from "../helpers/algorithms.js";
import { virosFormulaJPlus, calculateRotationNumber } from "../helpers/math.js";


class MyImmersion {
  /**
   * @type {MyDrawingView}
   */
  #parentView;

  /**
   * @type {Map<VisualsImmersion,boolean>}
   */
  #visualsImmersionBools;

  /**
   * @type {paper.Path}
   */
  #path;
  /**
   * @type {paper.Group}
   */
  #visibleElementsGroup;

  #name; // string // TODO ALEX: use or delete?

  // visible elements other than the path
  /**
   * @type {paper.Path.Circle[]}
   */
  #selfIntersectionMarkers;
  /**
   * @type {paper.PointText[]}
   */
  #pathPartNames;
  /**
   * @type {paper.PointText[]}
   */
  #connectedComponentWindingNumberLabels; // array of paper.PointText
  /**
   * @type {paper.Path[]}
   */
  #connectedComponentAreaPaths;

  // calculated stuff
  /**
   * @type {boolean}
   */
  #noTangentialCrossings;
  /**
   * @type {integer} even number
   */
  #jPlus;
  /**
   * @type {integer}
   */
  #rotationNumber; // int

  // children elements
  /**
   * @type {MySelfIntersection[]}
   */
  #selfIntersections;
  /**
   * @type {MyImmersionPart[]}
   */
  #immersionParts;
  /**
   * @type {MyConnectedComponent[]}
   */
  #connectedComponents;

  /**
   * @type {boolean}
   */
  #finishedPathProcessing;

  // generation helpers
  /**
   * @type {MyBifurcationPreparation}
   */
  #bifurcationPreparations;


  /**
   * @param {MyDrawingView} parentView
   * @param {paper.Point[]} startingPointOrPoints array of objects that has elements x and y, representing the coordinates as integer values. Can also be a single point, not an array.
   * @param {boolean} visible
   * @param {Map<VisualsImmersion,boolean>} visualsImmersionBools
   */
  constructor(parentView, startingPointOrPoints=undefined, visualsImmersionBools=(new Map())) {
    this.#parentView = parentView;
    this.#visualsImmersionBools = visualsImmersionBools;

    this.#initializePath();
    this.#initializePathPoints(startingPointOrPoints);
    this.#initializeVisibleElementsGroup(
      this.#getVisualsBool(VisualsImmersion.PATH)
    );
  }

  #initializePath() {
    this.#path = new paper.Path();
    this.#path.strokeColor = this.parentViewSettings.DRAWING_IMMERSION_STROKE_COLOR;
    this.#path.strokeWidth = this.parentViewSettings.DRAWING_IMMERSION_STROKE_WIDTH;
    if (this.parentViewSettings.IMMERSION_CLICKABLE_IN_NON_ZERO_CC) {
      this.#path.fillColor = "#ffffff01"; // with this, there is a almost completely transparent white fill (for all connected components with winding number not 0), which makes the immersion easier to select with the mouse mode select. This is only necessary for when the cc areas are toggled to be invisible.
    }
  }

  #initializePathPoints(startingPointOrPoints) {
    if (startingPointOrPoints !== undefined) {
      if (Array.isArray(startingPointOrPoints)) {
        this.addPathPoints(startingPointOrPoints);
      } else {
        this.addPathPoint(startingPointOrPoints);
      }
    }
  }

  #initializeVisibleElementsGroup(pathVisible) {
    this.#visibleElementsGroup = new paper.Group();
    this.addVisibleElement(this.#path);

    this.#path.visible = pathVisible;

    this.#name = `immersion-${this.#visibleElementsGroup.id}`;
  }

  destroySelf() {
    this.#path.remove();
    this.#visibleElementsGroup.remove();
    const children = [
      ...this.#selfIntersections || [],
      ...this.#immersionParts || [],
      ...this.#connectedComponents || [],
    ];
    children.map(child => child.destroySelf());
  }


  // custom getters

  /**
   * @returns {Map<VisualsImmersion,boolean>}
   */
  get visualsImmersionBools() {
    return this.#visualsImmersionBools;
  }

  get groupId() {
    return this.#visibleElementsGroup.id;
  }

  /**
   * @returns {boolean}
   */
  get validImmersion() {
    return this.noTangentialCrossings;
  }

  /**
   * @returns {boolean}
   */
  get closed() {
    return this.#path.closed;
  }

  /**
   * @returns {paper.Point}
   */
  get center() {
    return this.#path.bounds.center;
  }

  /**
   * @returns {paper.Path}
   */
  get pathClone() {
    return this.#path.clone();
  }

  /**
   * @returns {boolean}
   */
  get noTangentialCrossings() {
    this.checkCrossingsAndIntersections();
    return this.#noTangentialCrossings;
  }

  /**
   * @returns {MySelfIntersection[]}
   */
  get selfIntersections() {
    this.checkCrossingsAndIntersections();
    return this.#selfIntersections;
  }
  /**
   * @returns {integer}
   */
  get selfIntersectionsCount() {
    return this.selfIntersections.length;
  }

  /**
   * @returns {MyImmersionPart[]}
   */
  get immersionParts() {
    this.#createImmersionPartsSeparatedBySelfIntersections();
    return this.#immersionParts;
  }
  /**
   * @returns {integer}
   */
  get immersionPartsCount() {
    return this.immersionParts.length;
  }

  /**
   * @returns {MyConnectedComponent[]}
   */
  get connectedComponents() {
    this.#createConnectedComponents();
    return this.#connectedComponents;
  }
  /**
   * @returns {integer}
   */
  get connectedComponentsCount() {
    return this.connectedComponents.length;
  }

  /**
   * @returns {integer[]}
   */
  get connectedComponentWindingNumbers() {
    return this.connectedComponents.map(cc => cc.windingNumber);
  }

  /**
   * @returns {integer[]}
   */
  get doublePointIndices() {
    return this.selfIntersections.map(sInt => sInt.doublePointIndex);
  }

  /**
   * @returns {integer}
   */
  get jPlus() {
    this.calculateJPlus();
    return this.#jPlus;
  }

  /**
   * @returns {integer}
   */
  get rotationNumber() {
    this.calculateRotationNumber();
    return this.#rotationNumber;
  }

  /**
   * @returns {MySettings}
   */
  get parentViewSettings() {
    return this.#parentView.settings;
  }

  /**
   * @param {VisualsImmersion}
   * @returns {boolean}
   */
  #getVisualsBool(visuals) {
    return this.#visualsImmersionBools.get(visuals.symbol)
  }


  // custom setters

  set lessVisible(bool) {
    this.opacity = bool ? this.parentViewSettings.NOT_SELECTION_CANDIDATE_OPACITY : 1;
  }
  set accentuated(bool) {
    this.#visibleElementsGroup.bounds.selected = bool;
  }

  /**
   * @param {number} value between 0 and 1
   */
  set opacity(value) {
    this.#visibleElementsGroup.opacity = value;
  }


  // simple wrapper stuff

  addPathPoint(newPoint) {
    this.#path.add(newPoint);
  }
  addPathPoints(newPoints) {
    this.#path.addSegments(newPoints);
  }

  closePath() {
    this.#path.closePath(); // see http://paperjs.org/reference/pathitem/#closepath
  }

  /**
   * @param {String} colorString
   */
  setStrokeColor(colorString) {
    this.#path.strokeColor = colorString;
  }
  /**
   * @param {number} width
   */
  setStrokeWidth(width) {
    this.#path.strokeWidth = width;
  }

  smoothenPath() {
    // this.#path.flatten(1000);
    // this.#path.smooth();
    this.#path.smooth({
      type: 'geometric',
      factor: 0.4,
    });
  }

  smoothenPathClosure() {
    this.#path.smooth({
      type: 'geometric',
      from: path.segments.length -1,
      to: 1,
    });
  }

  simplifyPath(
    tolerance = 2.5 // Number — the allowed maximum error when fitting the curves through the segment points — optional, default: 2.5
  ) {
    // see http://paperjs.org/reference/path/#simplify
    this.#path.simplify(tolerance);
  }

  getSmallerBoundsLength() {
    const boundingRectangle = this.#path.bounds;
    return Math.min(boundingRectangle.width, boundingRectangle.height);
  }

  resizeImmersion(scale) {
    this.#visibleElementsGroup.scale(scale); // TODO FIXME? this might have to recalculate some other stuff as well, like the children elements
  }

  bringToFront() {
    this.#visibleElementsGroup.bringToFront();
  }
  sendToBack() {
    this.#visibleElementsGroup.sendToBack();
  }


  // path manipulation stuff

  finishDrawing(
    applySmoothing,
    applySimplification,
    visualizeChildren
  ) {
    if (this.#path.segments.length < 2) {
      this.#parentView.handleImmersionDelete();
      this.destroySelf();
      return false;
    }

    this.closeImmersion(false);

    if (applySimplification) {
      // TODO ALEX CONTINUE fine tune this
      // const smallerBoundsLength = this.getSmallerBoundsLength();
      // this.simplifyPath(smallerBoundsLength * (this.parentViewSettings.DRAWN_PATH_SIMPLIFICATION_TOLERANCE_PERCENT / 100));
    }
    if (applySmoothing) {
      this.smoothenPath()
    }

    this.applySpecialPathProcessing(visualizeChildren);

    return true;
  }

  closeImmersion(smoothConnections) {
    this.closePath();

    if (smoothConnections) {
      this.smoothenPathClosure()
    }
  }

  /**
   * @param {{ x: number, y: number }} delta
   */
  moveImmersion(delta) {
    const currentPosition = this.#visibleElementsGroup.position;
    currentPosition.set(currentPosition.add(delta));

    // // old implementation:
    // this.#visibleElementsGroup.position.x += delta.x;
    // this.#visibleElementsGroup.position.y += delta.y;
  }


  // special path procedures, creating children

  resetPathProcessing(retriggerPathprocessing=false) { // TODO ALEX CONTINUE use this after path manipulation
    this.#finishedPathProcessing = false;

    [
      this.#selfIntersectionMarkers,
      this.#pathPartNames,
      this.#connectedComponentWindingNumberLabels,
      this.#connectedComponentAreaPaths,
    ].map(visibleElements => {
      visibleElements.map(el => this.removeVisibleElement(el));
    });

    // calculated stuff
    this.#noTangentialCrossings = undefined;
    this.#jPlus = undefined;
    this.#rotationNumber = undefined;

    // children elements
    this.#selfIntersections = undefined;
    this.#immersionParts = undefined;
    this.#connectedComponents = undefined;

    if (retriggerPathprocessing) {
      this.applySpecialPathProcessing(true);
    }
  }

  applySpecialPathProcessing(visualize) {
    if (this.#finishedPathProcessing) {
      return;
    }

    this.checkCrossingsAndIntersections();
    if (this.noTangentialCrossings) {
      this.#createImmersionPartsSeparatedBySelfIntersections();
      this.#createConnectedComponents();
    }

    if (visualize && this.validImmersion) {
      const getChecked = (vi) => (this.#parentView?.getVisualsImmersionCheckboxChecked(vi) ?? true);

      this.#showSelfIntersections(getChecked(VisualsImmersion.PATH_INTERSECTIONS)); // TODO ALEX CONTINUE rework
      this.#showPathPartNames(getChecked(VisualsImmersion.PATH_PARTS)); // TODO ALEX CONTINUE rework
      this.#showConnectedComponentAreas(
        getChecked(VisualsImmersion.CONNECTED_COMPONENT_AREAS),
        getChecked(VisualsImmersion.CONNECTED_COMPONENT_BOUNDARIES)
      ); // TODO ALEX CONTINUE rework
      this.#showConnectedComponentWindingNumbers(getChecked(VisualsImmersion.CONNECTED_COMPONENT_WINDING_NUMBERS)); // TODO ALEX CONTINUE rework
    }

    this.#finishedPathProcessing = true;
  }

  checkCrossingsAndIntersections() {
    if (this.#noTangentialCrossings !== undefined) {
      return;
    }

    const crossings = this.#path.getCrossings(this.#path);
    const intersections = this.#path.getIntersections(this.#path);

    this.#noTangentialCrossings = (crossings.length === intersections.length);
    if (this.#noTangentialCrossings === false) {
      this.#selfIntersections = null;

      console.log({intersections});
      console.log({crossings});

      const intsNoCrossing = intersections
        .filter(intx => crossings
          .filter(cr => cr.point.equals(intx.point)).length === 0
        );
      const crsNoIntx = crossings
        .filter(cr => intersections
          .filter(intx => intx.point.equals(cr.point)).length === 0
        );

      console.log({intsNoCrossing});
      console.log({crsNoIntx});

      intsNoCrossing.forEach(intx => visualizePointWithCircle(intx.point, "cyan"));
      crsNoIntx.forEach(intx => visualizePointWithCircle(intx.point, "orange"));
    }

    const selfInts = crossings.map(cr => new MySelfIntersection(this, cr))
    this.#selfIntersections = selfInts;
  }

  #createImmersionPartsSeparatedBySelfIntersections() {
    if (this.#immersionParts !== undefined) {
      return;
    }

    if (this.noTangentialCrossings !== true) {
      console.error('createImmersionPartsSeparatedBySelfIntersections should not be called if curve has tangential crossings')
      return;
    }

    this.#immersionParts = createPartsFromPathIntersections(this);
  }

  #createConnectedComponents() {
    if (this.#connectedComponents !== undefined) {
      return;
    }

    if (this.immersionParts === undefined) {
      console.error('createConnectedComponents should not be called if there are no immersionParts yet')
      return;
    }

    this.#connectedComponents = createConnectedComponentsFromPathParts(
      this,
      this.parentViewSettings.LOOP_LIMIT,
      this.parentViewSettings.EPSILON_TANGENT_COMPARISON
    );
  }


  // visualization of visible elements

  /**
   * @param {paper.Item} newElement
   */
  addVisibleElement(newElement) {
    this.#visibleElementsGroup.addChild(newElement)
  }
  /**
   * @param {paper.Item[]} newElement
   */
  addVisibleElements(newItems) {
    this.#visibleElementsGroup.addChildren(newItems);
  }
  /**
   * @param {paper.Item} removeElement
   */
  removeVisibleElement(removeElement) {
    if (this.#visibleElementsGroup.isChild(removeElement)) {
      const childIndex = removeElement.index;
      this.#visibleElementsGroup.removeChildren(childIndex, childIndex+1);
      removeElement.remove();
    } else {
      console.warn('removeVisibleElement: tried to remove element that is not a child of the immersion\'s visibleElementsGroup')
    }
  }

  handleVisualsPath(visible) {
    paperItemVisibility(this.#path, visible);
    this.#visualsImmersionBools.set(VisualsImmersion.PATH.symbol, visible);
  }
  handleVisualsPathIntersections(visible) {
    this.#selfIntersectionMarkers.map(el => paperItemVisibility(el, visible));
    this.#visualsImmersionBools.set(VisualsImmersion.PATH_INTERSECTIONS.symbol, visible);
  }
  handleVisualsPathSegmentsAndHandles(visibleSegments, visibleHandles) {
    this.#path.selected = false;
    this.#path.fullySelected = false;

    if (visibleHandles) {
      this.#path.fullySelected = visibleHandles;
    } else {
      this.#path.selected = visibleSegments;
    }
    this.#visualsImmersionBools.set(VisualsImmersion.PATH_SEGMENTS.symbol, visibleSegments);
    this.#visualsImmersionBools.set(VisualsImmersion.PATH_SEGMENT_HANDLES.symbol, visibleHandles);
  }
  handleVisualsPathParts(visible) {
    this.#pathPartNames.map(el => paperItemVisibility(el, visible));
    // TODO ALEX path parts colors
    this.#visualsImmersionBools.set(VisualsImmersion.PATH_PARTS.symbol, visible);
  }
  handleVisualsPathOrientation(visible) {
    // TODO ALEX path orientation
  }
  handleVisualsCCAreas(visibleArea) {
    this.#connectedComponentAreaPaths.map(cc => {
      const currentColor = cc.fillColor;
      const colorString = rgbToHex(currentColor.red, currentColor.green, currentColor.blue);
      cc.fillColor = colorString.substring(0, 7) + (visibleArea ? '70' : '00');
      cc.bringToFront();
    })
    this.#visualsImmersionBools.set(VisualsImmersion.CONNECTED_COMPONENT_AREAS.symbol, visibleArea);
  }
  handleVisualsCCBoundaries(visibleBoundary) {
    this.#connectedComponentAreaPaths.map(cc => {
      const currentColor = cc.strokeColor;
      const colorString = rgbToHex(currentColor.red, currentColor.green, currentColor.blue);
      cc.strokeColor = colorString.substring(0, 7) + (visibleBoundary ? 'ff' : '00');
      cc.bringToFront();
    })
    this.#visualsImmersionBools.set(VisualsImmersion.CONNECTED_COMPONENT_BOUNDARIES.symbol, visibleBoundary);
  }
  handleVisualsCCWindingNumbers(visible) {
    this.#connectedComponentWindingNumberLabels.map(el => paperItemVisibility(el, visible));
    this.#visualsImmersionBools.set(VisualsImmersion.CONNECTED_COMPONENT_WINDING_NUMBERS.symbol, visible);
  }
  handleVisualsImmersionDPIndices(visible) {
    // this.#selfIntersections.map(si => ...); // TODO ALEX
    this.#visualsImmersionBools.set(VisualsImmersion.DOUBLE_POINT_INDICES.symbol, visible);
  }


  // mathematical calculations

  calculateJPlus() {
    if (this.#jPlus !== undefined) {
      return;
    }
    this.applySpecialPathProcessing(false);

    if (this.checkReadyToCalculateJPlus()) {
      this.#jPlus = virosFormulaJPlus(
        this.connectedComponentWindingNumbers,
        this.doublePointIndices
      )
    }
  }

  calculateRotationNumber() {
    if (this.#rotationNumber !== undefined) {
      return;
    }
    this.applySpecialPathProcessing(false);

    if (this.checkReadyToCalculateJPlus()) {
      this.#rotationNumber = calculateRotationNumber(
        this.connectedComponentWindingNumbers,
        this.doublePointIndices
      )
    }
  }

  checkReadyToCalculateJPlus() {
    if (!this.checkSelfIntersectionsAndConnectedComponentsAmountMatch()) {
      console.warn(
        'MyImmersion calculateJPlus: number of connectedComponents and selfIntersections do not match. There should be two more intersections than there are connected components.',
        this.connectedComponents,
        this.selfIntersections
      );
      return false;
    }

    if (!this.checkAllConnectedComponentsHaveIntegerWindingNumbers()) {
      console.warn(
        'MyImmersion calculateJPlus: not all windingNumbers from connectedComponents available',
        connectedComponentWindingNumbers,
        this.connectedComponents.length
      );
      return false;
    }

    if (!this.checkAllSelfIntersectionsHaveDoublePointIndex()) {
      console.warn(
        'MyImmersion calculateJPlus: not all doublePointIndices from selfIntersections available',
        doublePointIndices,
        this.selfIntersections.length
      );
      return false;
    }

    return true;
  }

  checkSelfIntersectionsAndConnectedComponentsAmountMatch() {
    return this.selfIntersections.length === this.connectedComponents.length - 2;
  }

  checkAllConnectedComponentsHaveIntegerWindingNumbers() {
    const connectedComponentWindingNumbers = this.connectedComponents
      .map(cc => cc.windingNumber)
      .filter(wn => Number.isInteger(wn));
    return (connectedComponentWindingNumbers.length === this.connectedComponents.length);
  }

  checkAllSelfIntersectionsHaveDoublePointIndex() {
    const doublePointIndices = this.selfIntersections
      .map(sInt => sInt.doublePointIndex)
      .filter(dpi => Number.isInteger(dpi));
    return (doublePointIndices.length === this.selfIntersections.length);
  }


  // visualization, some mainly used for debugging

  #showSelfIntersections(visible=false) {
    const intersectionMarkerItems = this.selfIntersections
      .map(selfIntersection => visualizePointWithCircle(
        selfIntersection.point,
        this.parentViewSettings.VISUALS_IMMERSION_INTERSECTIONS_COLOR,
        this.parentViewSettings.VISUALS_IMMERSION_INTERSECTIONS_RADIUS,
        visible
      ));
    this.#selfIntersectionMarkers = intersectionMarkerItems;
    this.addVisibleElements(intersectionMarkerItems);
  }

  /**
   * @param {number} distance
   * @param {number} angle in degrees
   */
  #showPathPartNames(visible=false, distance=10, angle=0) {
    const partNames = this.immersionParts.map(part => {
      const middlePoint = part.middlePoint;
      const textOffsetDistance = (new paper.Point(distance, 0)).rotate(angle);

      return new paper.PointText({
        content: part.debuggingName,
        point: middlePoint.add(textOffsetDistance),
        fillColor: 'orange',
        fontWeight: 'bold',
        fontSize: 10,
        visible: visible,
      });
    })

    this.#pathPartNames = partNames;
    this.addVisibleElements(partNames);
  }

  #showConnectedComponentWindingNumbers(visible=false) {
    const windingNumberLabels = this.connectedComponents
      .map(component => {
        if (!component.isUnboundedComponent) {
          const someInnerPoint = getInnerPointInPathArea(component.boundaryPath);

          return new paper.PointText({
            // content: component.debuggingName,
            content: component.windingNumber,
            point: someInnerPoint,
            fillColor: 'blue',
            fontWeight: FontWeight.BOLD,
            fontFamily: FontFamily.COURIER_NEW,
            fontSize: 20,
            visible: visible,
          });
        }
      })
      .filter(cc => cc !== undefined);

    this.#connectedComponentWindingNumberLabels = windingNumberLabels;
    this.addVisibleElements(windingNumberLabels);
  }

  #showConnectedComponentAreas(visibleArea=false, visibleBoundary=false) {
    const randomIndex = Math.floor(Math.random() * COLOR_PALETTE_AREA.length);

    const areaPaths = this.connectedComponents
      .map((component, index) => {
        if (!component.isUnboundedComponent) {
          const randomColor = COLOR_PALETTE_AREA[properMod(randomIndex + index, COLOR_PALETTE_AREA.length)];

          const areaPath = component.boundaryPath;

          areaPath.visible = true;
          areaPath.strokeWidth = this.parentViewSettings.VISUALS_CC_AREAS_STROKE_WIDTH;
          areaPath.strokeColor = randomColor + (visibleBoundary ? 'ff' : '00');
          areaPath.fillColor = randomColor + (visibleArea ? componentToHex(this.parentViewSettings.VISUALS_CC_AREAS_ALPHA) : '00');

          return areaPath;
        }
      })
      .filter(cc => cc !== undefined);

    this.#connectedComponentAreaPaths = areaPaths;
    this.addVisibleElements(areaPaths);
  }


  // generate stuff

  startBifurcationPreparations() {
    if (this.#bifurcationPreparations !== undefined) {
      return;
    }
    this.#bifurcationPreparations = new MyBifurcationPreparation(this);
  }
  /**
   * @param {integer} bifNumber >= 1
   */
  setBifurcationNumber(bifurcationNumber) {
    this.#bifurcationPreparations.bifurcationNumber = bifurcationNumber;
  }
  /**
   * @returns {integer} bifNumber >= 1
   */
  getBifurcationNumber() {
    return this.#bifurcationPreparations.bifurcationNumber;
  }
  /**
   * @returns {paper.Path}
   */
  getBifurcationFromPreparations() {
    return this.#bifurcationPreparations.finishedBifurcation;
  }
}

export default MyImmersion;
