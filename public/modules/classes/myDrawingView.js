import MyGUI from "./myGui.js";
import MyImmersion from "./myImmersion.js";
import MyMouse from "./myMouse.js";
import MySettings from "./mySettings.js";
import MyCanvasInfo from "./myCanvasInfo.js";

import { SHORTCUT_KEYS } from "../constants/config.js";
import { VisualsView } from "../enums/visuals.js";
import { getClassForMouseMode, MouseMode } from "../enums/mouseMode.js";
import { Generate } from "../enums/generate.js";

import {
  smoothScaling,
  last,
  getMapValuesAsArray
} from "../helpers/basic.js";
import {
  visualizePointWithMarkerBullsEye,
  paperItemVisibility,
} from "../helpers/paper.js"
import { bifurcationJPlusLowerBoundFormula, bifurcationRotationNumberFormula } from "../helpers/math.js";


class MyDrawingView {
  #htmlCanvasElement;

  /**
   * @type {MySettings}
   */
  #mySettings;
  /**
   * @type {MyGUI}
   */
  #gui;

  /**
   * @type {paper.Project}
   */
  #paperProject;
  /**
   * @type {paper.View}
   */
  #paperView;
  #paperSettings; // Object, see http://paperjs.org/reference/paperscope/#settings

  #visibleElementsGroup; // paper.Group

  /**
   * @type {MyMouse}
   */
  #mouse;

  // immersion handling
  /**
   * @type {Map<number, MyImmersion>}
   */
  #immersions = new Map();
  /**
   * @type {MyImmersion}
   */
  #currentImmersion;

  // mathematical background element points
  /**
   * @type {paper.Point}
   */
  #regularizationOrigin; // paper.Point
  /**
   * @type {paper.Point}
   */
  #regularizationTarget; // paper.Point
  /**
   * @type {paper.Point}
   */
  #regularizationTwoCenterE; // paper.Point
  /**
   * @type {paper.Point}
   */
  #regularizationTwoCenterM; // paper.Point

  // mathematical background element points visualized
  /**
   * @type {paper.Point}
   */
  #regularizationOriginVisualization; // paper.Point
  /**
   * @type {paper.Point}
   */
  #regularizationTargetVisualization; // paper.Point
  /**
   * @type {paper.Point}
   */
  #regularizationTwoCenterEVisualization; // paper.Point
  /**
   * @type {paper.Point}
   */
  #regularizationTwoCenterMVisualization; // paper.Point

  // mathematical background element points derivatives
  /**
   * @type {paper.Path.Line}
   */
  #regularizationTwoCenterEMLine; // paper.Path.Line
  /**
   * @type {paper.Path.Line}
   */
  #regularizationXPositive; // paper.Path.Line

  // mathematical regularization branch visualizations
  #regularizationLCBranches; // array of paper.Path // TODO ALEX this should be part of the MyImmersion-object, not of the view
  #regularizationBirkhoffBranches; // array of paper.Path // TODO ALEX this should be part of the MyImmersion-object, not of the view

  // obsolete stuff
  /**
   * @type {MyCanvasInfo}
   */
  #canvasTextItem; // MyCanvasInfo

  /**
   * @param {string} htmlCanvasId
   * @param {MyGUI} gui
   * @param {MySettings} mySettings
   */
  constructor(htmlCanvasId, gui, mySettings) {
    this.#mySettings = mySettings;
    this.#gui = gui;
    this.#gui.canvasView = this;

    this.#essentialSetup(htmlCanvasId);
    this.#additionalSetup();
    this.#initializeBackgroundElements(
      VisualsView.ORIGIN.default,
      VisualsView.TARGET.default,
      VisualsView.TWO_CENTER_E.default,
      VisualsView.TWO_CENTER_M.default,
      VisualsView.TWO_CENTER_EM_LINE_SEGMENT.default,
      VisualsView.X_POSITIVE_RAY.default
    );
    this.#extraSetup();
  }

  // initialization stuff

  #essentialSetup(htmlCanvasId) {
    // // useful for early testing, but overrides global stuff so easier not to use this:
    // paper.install(window); // http://paperjs.org/reference/paperscope/#install-scope

    paper.setup(htmlCanvasId); // http://paperjs.org/reference/paperscope/#setup-element
    this

    this.#htmlCanvasElement = document.getElementById(htmlCanvasId);
    this.#paperProject = paper.project;
    this.#paperView = paper.view;
    this.#paperSettings = paper.settings;
  }

  #additionalSetup() {
    this.#mouse = new MyMouse(this);
    this.addCanvasMouseModeCss(this.#mouse.mode);
    this.#canvasTextItem = new MyCanvasInfo();
    this.#paperView.onKeyDown = (e) => this.onKeyDown(e, this);
  }

  #initializeBackgroundElements(
    originVisible=false,
    targetVisible=false,
    twoCenterEVisible=false,
    twoCenterMVisible=false,
    twoCenterEMLineVisible=false,
    xPositiveLineVisible=false
  ) {
    this.#visibleElementsGroup = new paper.Group();
    this.#visibleElementsGroup.visible = true;
    this.#visibleElementsGroup.locked = true;

    const { x: centerX, y: centerY } = this.#paperView.center;
    const { width: viewWidth } = this.#paperView.viewSize;

    const quarterWidth = viewWidth / 4;
    const sixthWidth = viewWidth / 6;
    const originX = centerX - quarterWidth;
    const targetX = centerX + quarterWidth;

    this.#regularizationOrigin = new paper.Point(originX, centerY);
    this.#regularizationTarget = new paper.Point(targetX, centerY);
    this.#regularizationTwoCenterE = new paper.Point(originX - sixthWidth, centerY);
    this.#regularizationTwoCenterM = new paper.Point(originX + sixthWidth, centerY);

    this.#regularizationOriginVisualization = visualizePointWithMarkerBullsEye(
      this.#regularizationOrigin,
      this.#mySettings.VISUALS_REGULARIZATION_POINTS_RADIUS,
      this.#mySettings.VISUALS_REGULARIZATION_POINTS_COLOR,
      originVisible,
      "0"
    );
    this.#regularizationTargetVisualization = visualizePointWithMarkerBullsEye(
      this.#regularizationTarget,
      this.#mySettings.VISUALS_REGULARIZATION_POINTS_RADIUS,
      this.#mySettings.VISUALS_REGULARIZATION_POINTS_COLOR,
      targetVisible,
      "T"
    );
    this.#regularizationTwoCenterEVisualization = visualizePointWithMarkerBullsEye(
      this.#regularizationTwoCenterE,
      this.#mySettings.VISUALS_REGULARIZATION_POINTS_RADIUS,
      this.#mySettings.VISUALS_REGULARIZATION_POINTS_COLOR,
      twoCenterEVisible,
      "E"
    );
    this.#regularizationTwoCenterMVisualization = visualizePointWithMarkerBullsEye(
      this.#regularizationTwoCenterM,
      this.#mySettings.VISUALS_REGULARIZATION_POINTS_RADIUS,
      this.#mySettings.VISUALS_REGULARIZATION_POINTS_COLOR,
      twoCenterMVisible,
      "M"
    );

    this.#addVisibleElements([
      this.#regularizationOriginVisualization,
      this.#regularizationTargetVisualization,
      this.#regularizationTwoCenterEVisualization,
      this.#regularizationTwoCenterMVisualization
    ]);

    this.resetXPositiveAndEMLine(twoCenterEMLineVisible, xPositiveLineVisible);

    // this.#regularizationLCBranches; // TODO ALEX (probably not here)
    // this.#regularizationBirkhoffBranches; // TODO ALEX (probably not here)
  }

  #extraSetup() {
    this.#setCurveHandleSize(this.#mySettings.CURVE_HANDLE_SIZE);
    // this.#createTestCurves();
  }

  #setCurveHandleSize(sizeInt) {
    // see http://paperjs.org/reference/paperscope/#settings
    this.#paperSettings.handleSize = sizeInt;
  }

  // #createTestCurves() {
  //   const testImmersion = this.createNewImmersion(examplePolygonalCurveNeckPillow.points);
  //   testImmersion.finishDrawing(false, false, true);
  // }


  // custom getters, setters

  get settings() {
    return this.#mySettings;
  }

  get paperProject() {
    return this.#paperProject;
  }

  /**
   * @returns {MyImmersion}
   */
  get currentImmersion() {
    return this.#currentImmersion;
  }

  /**
   * @param {VisualsView} visualsView
   * @returns {boolean}
   */
  getVisualsImmersionCheckboxChecked(visualsView) {
    return this.#gui.getVisualsImmersionCheckboxChecked(visualsView);
  }

  /**
   * @param {MyImmersion} selectedImmersion
   */
  set currentImmersion(selectedImmersion) {
    if (this.#currentImmersion !== selectedImmersion) {
      this.#currentImmersion = selectedImmersion;
      this.handleImmersionSelectedOrFinished();

      // switch generate radio to Nothing-selection
      this.#gui.handleGenerateRadioChecked(Generate.NOTHING);
    }
  }

  handleImmersionSelectedOrFinished() {
    this.#updateCanvasAfterImmersionSelected();
    this.#updateUIAfterImmersionSelected();
  }
  #updateCanvasAfterImmersionSelected() {
    if (this.#mouse.mode === MouseMode.IMMERSION_SELECT) {
      this.handleImmersionSelectionCandidatesMode(true);
    }
  }
  #updateUIAfterImmersionSelected() {
    const selectedImmersion = this.#currentImmersion;

    if (selectedImmersion === undefined) {
      this.#gui.handleNoImmersionSelected();
    } else {
      this.#gui.handleImmersionSelected(selectedImmersion);
    }
  }

  /**
   *
   * @param {MouseMode} mouseMode
   */
  handleMouseRadioChecked(mouseMode) {
    this.#gui.handleMouseRadioChecked(mouseMode);
  }

  get newestImmersion() {
    const availableImmersions = this.availableImmersions;
    if (availableImmersions.length > 0) {
      return last(availableImmersions);
    } else {
      undefined;
    }
  }

  get availableImmersions() {
    return getMapValuesAsArray(this.#immersions);
  }
  get immersionsCount() {
    return this.availableImmersions.length;
  }


  // visualization of visible elements

  /**
   * @param {paper.Item} newElement
   */
  #addVisibleElement(newElement) {
    this.#visibleElementsGroup.addChild(newElement)
  }
  /**
   * @param {paper.Item[]} newElement
   */
  #addVisibleElements(newItems) {
    this.#visibleElementsGroup.addChildren(newItems);
  }
  /**
   * @param {paper.Item} removeElement
   */
  #removeVisibleElement(removeElement) {
    if (this.#visibleElementsGroup.isChild(removeElement)) {
      const childIndex = removeElement.index;
      this.#visibleElementsGroup.removeChildren(childIndex, childIndex+1);
      removeElement.remove();
    } else {
      console.warn('removeVisibleElement: tried to remove element that is not a child of the immersion\'s visibleElementsGroup')
    }
  }

  resetXPositiveAndEMLine(
    twoCenterEMLineVisible=false,
    xPositiveLineVisible=false
  ) {
    if (this.#regularizationTwoCenterEMLine !== undefined) {
      this.#removeVisibleElement(this.#regularizationTwoCenterEMLine);
    }

    this.#regularizationTwoCenterEMLine = new paper.Path.Line({
      from: this.#regularizationTwoCenterE,
      to: this.#regularizationTwoCenterM,
      strokeWidth: this.#mySettings.VISUALS_REGULARIZATION_EM_LINE_STROKE_WIDTH,
      strokeColor: this.#mySettings.VISUALS_REGULARIZATION_EM_LINE_COLOR,
      visible: twoCenterEMLineVisible,
    });
    this.#addVisibleElement(this.#regularizationTwoCenterEMLine);


    if (this.#regularizationXPositive !== undefined) {
      this.#removeVisibleElement(this.#regularizationXPositive);
    }

    const { width: viewWidth, height: viewHeight } = this.#paperView.viewSize;
    const biggerViewMeasurement = Math.max(viewWidth, viewHeight);
    const multiplicator = 64; // some big number, for fun a power of 2
    const lineVector = this.#regularizationTwoCenterM.subtract(this.#regularizationOrigin);
    const lineEndPoint = this.#regularizationOrigin.add(
      lineVector.multiply(
        Math.floor(biggerViewMeasurement * multiplicator)
      )
    );

    this.#regularizationXPositive = new paper.Path.Line({
      from: this.#regularizationOrigin,
      to: lineEndPoint,
      strokeWidth: this.#mySettings.VISUALS_REGULARIZATION_X_POSITIVE_LINE_STROKE_WIDTH,
      strokeColor: this.#mySettings.VISUALS_REGULARIZATION_X_POSITIVE_LINE_COLOR,
      visible: xPositiveLineVisible,
    });
    this.#addVisibleElement(this.#regularizationXPositive);

    this.#regularizationTwoCenterEMLine.bringToFront();
  }


  // immersion handling stuff

  /**
   * @param {paper.Point | paper.Point[]} startingPointOrPoints
   * @returns {MyImmersion}
   */
  createNewImmersion(startingPointOrPoints) {
    const newImmersion = new MyImmersion(
      this,
      startingPointOrPoints,
      this.#gui.visualsImmersionBools
    );
    this.#immersions.set(newImmersion.groupId, newImmersion);

    this.currentImmersion = newImmersion;

    return newImmersion;
  }

  /**
   * @param {paper.Path} closedPath
   */
  createNewImmersionFromClosedPath(closedPath, selectNewImmersion=true) {
    const newImmersion = new MyImmersion(
      this,
      closedPath.segments,
      this.#gui.visualsImmersionBools
    );
    newImmersion.finishDrawing(false, false, true);
    this.#immersions.set(newImmersion.groupId, newImmersion);

    if (selectNewImmersion) {
      this.currentImmersion = newImmersion;
    }
  }

  /**
   * @param {number} groupId id of the MyImmersion.visibleElementsGroup (type paper.Group)
   * @returns {MyImmersion}
   */
  findImmersionByGroupId(groupId) {
    return this.availableImmersions
      .filter(im => im.groupId === groupId)[0];
  }

  deleteAllImmersions() {
    getMapValuesAsArray(this.#immersions)
      .map(im => im.destroySelf());
    this.#immersions.clear();
  }
  #deleteCurrentImmersion() {
    this.#immersions.delete(this.#currentImmersion.groupId);
    this.#currentImmersion.destroySelf();

    this.currentImmersion = this.newestImmersion;

    if (this.currentImmersion === undefined) {
      this.#handleNoImmersionsLeft();
    }
  }


  // event handlers

  /**
   * @param {paper.KeyEvent} event
   * @param {MyDrawingView} self
   */
  onKeyDown(event, self) {
    switch (event.key) {
      case SHORTCUT_KEYS.RESET.toLowerCase():
        self.resetCanvas();
        break;
      // case SHORTCUT_KEYS.SELECT.toLowerCase():
      //   togglePathHandleVisualization();
      //   break;
      // case SHORTCUT_KEYS.RESIZE_BIGGER.toLowerCase():
      //   self.resizeCurrentImmersion(true);
      //   break;
      // case SHORTCUT_KEYS.RESIZE_SMALLER.toLowerCase():
      //   self.resizeCurrentImmersion(false);
      //   break;
      case SHORTCUT_KEYS.RESIZE_BIGGER.toLowerCase():
        self.zoomView(true);
        break;
      case SHORTCUT_KEYS.RESIZE_SMALLER.toLowerCase():
        self.zoomView(false);
        break;
      case SHORTCUT_KEYS.TEST_STUFF.toLowerCase():
        // console.log(self.#paperView.viewSize);
        // console.log(self.#paperView.center);
        break;
      case '1':
        this.#temporaryHelperMethodForBifurcationtesting(1);
        break;
      case '2':
        this.#temporaryHelperMethodForBifurcationtesting(2);
        break;
      case '3':
        this.#temporaryHelperMethodForBifurcationtesting(3);
        break;
      case '4':
        this.#temporaryHelperMethodForBifurcationtesting(4);
        break;
      case '5':
        this.#temporaryHelperMethodForBifurcationtesting(5);
        break;
      case '6':
        this.#temporaryHelperMethodForBifurcationtesting(6);
        break;
      case '7':
        this.#temporaryHelperMethodForBifurcationtesting(7);
        break;
      case '8':
        this.#temporaryHelperMethodForBifurcationtesting(8);
        break;
      case '9':
        this.#temporaryHelperMethodForBifurcationtesting(9);
        break;
      case SHORTCUT_KEYS.BIFURCATE.toLowerCase():
        this.currentImmersion.startBifurcationPreparations();

        const beforeJPlus = this.currentImmersion.jPlus;
        const beforeRotationNumber = this.currentImmersion.rotationNumber;
        const bifurcationNumber = this.currentImmersion.getBifurcationNumber();
        console.log({jplusBefore: beforeJPlus, rotationBefore: beforeRotationNumber})
        console.log({
          jplusPredictionAfter: bifurcationJPlusLowerBoundFormula(beforeJPlus, bifurcationNumber),
          rotationPredictionAfter: bifurcationRotationNumberFormula(beforeRotationNumber, bifurcationNumber)
        })

        this.createNewImmersionFromClosedPath(this.currentImmersion.getBifurcationFromPreparations(), false);
        this.#deleteCurrentImmersion();

        console.log({jplusAfter: this.currentImmersion.jPlus, rotationAfter: this.currentImmersion.rotationNumber})
        break;
    }
  }
  #temporaryHelperMethodForBifurcationtesting(bifNumber) {
    this.currentImmersion.startBifurcationPreparations();
    this.#currentImmersion.setBifurcationNumber(bifNumber);
  }


  // external event handlers

  resetCanvas() {
    // TODO ALEX 456: try here creating new session objects MyGUI and MyDravingView, maybe this even restores the session when an error crashed the program
    this.deleteAllImmersions();
    this.currentImmersion = this.newestImmersion;
    this.#handleNoImmersionsLeft();
  }

  handleImmersionSelectionCandidatesMode(modeActive) {
    if (modeActive) {
      this.availableImmersions.map(im => {
        im.lessVisible = (im !== this.#currentImmersion);
        im.accentuated = false;
      });
    } else {
      this.availableImmersions.map(im => {
        im.lessVisible = false;
        im.accentuated = false;
      });
    }
  }

  #handleNoImmersionsLeft() {
    this.handleChangeMouseMode(MouseMode.IMMERSION_NEW);
  }

  // updateCanvasText(newText) {
  //   this.#canvasTextItem.updateCanvasText(newText)
  // }

  /**
   * @param {boolean} resizeBigger
   */
  resizeCurrentImmersion(resizeBigger) {
    if (this.currentImmersion === undefined) {
      return;
    }
    const baseScaling = this.#mySettings.RESIZE_SCALE;
    const scaleFactor = resizeBigger ? baseScaling : (1 / baseScaling);

    smoothScaling(
      (factor) => this.currentImmersion.resizeImmersion(factor),
      scaleFactor,
      50
    );
  }

  /**
   * @param {{ x: number, y: number }} delta
   */
  moveView(delta) {
    this.#paperView.translate(delta);
  }

  /**
   * @param {boolean} resizeBigger
   */
  zoomView(resizeBigger) {
    const baseScaling = this.#mySettings.ZOOM_SCALE;
    const scaleFactor = resizeBigger ? baseScaling : (1 / baseScaling);

    // this.#paperView.scale(scaleFactor);
    smoothScaling(
      (factor) => this.#paperView.scale(factor),
      scaleFactor,
      50
    );
  }

  /**
   * @param {{ x: number, y: number }} delta
   */
  moveCurrentImmersion(delta) {
    this.currentImmersion.moveImmersion(delta);
  }


  /**
   * @param {MouseMode} newMode
   */
  handleChangeMouseMode(newMode) {
    this.#mouse.mode = newMode;
    this.handleImmersionSelectionCandidatesMode(newMode === MouseMode.IMMERSION_SELECT);
  }
  /**
   * @param {Generate} newMode
   */
  handleChangeGenerate(newGenerate) {
    switch (newGenerate) {
      case Generate.NOTHING:
        // TODO ALEX maybe disable view of the bifurcation preparations
        break;
      case Generate.BIFURCATION:
        if (this.currentImmersion !== undefined) {
          this.currentImmersion.startBifurcationPreparations();
        }
        break;
      case Generate.ROTATED_KEPLER_ELLIPSE:
        break;
      case Generate.LEVI_CIVITA_REGULARIZATION:
        break;
      case Generate.BIRKHOFF_REGULARIZATION:
        break;
    }
  }
  /**
   * @param {MouseMode} mouseMode
   * @param {boolean} isClicked
   */
  removeCanvasMouseModeCss(mouseMode, isClicked=false) {
    this.#htmlCanvasElement.classList.remove(getClassForMouseMode(mouseMode, isClicked));
  }
  /**
   * @param {MouseMode} mouseMode
   * @param {boolean} isClicked
   */
  addCanvasMouseModeCss(mouseMode, isClicked=false) {
    this.#htmlCanvasElement.classList.add(getClassForMouseMode(mouseMode, isClicked));
  }

  handleVisualsImmersionPath(visible) {
    this.currentImmersion?.handleVisualsPath(visible);
  }
  handleVisualsImmersionPathIntersections(visible) {
    this.currentImmersion?.handleVisualsPathIntersections(visible);
  }
  handleVisualsImmersionPathSegmentsAndHandles(visibleSegments, visibleHandles) {
    this.currentImmersion?.handleVisualsPathSegmentsAndHandles(visibleSegments, visibleHandles);
  }
  handleVisualsImmersionPathParts(visible) {
    this.currentImmersion?.handleVisualsPathParts(visible);
  }
  handleVisualsImmersionPathOrientation(visible) {
    this.currentImmersion?.handleVisualsPathOrientation(visible);
  }
  handleVisualsImmersionCCAreas(visible) {
    this.currentImmersion?.handleVisualsCCAreas(visible);
  }
  handleVisualsImmersionCCBoundaries(visible) {
    this.currentImmersion?.handleVisualsCCBoundaries(visible);
  }
  handleVisualsImmersionCCWindingNumbers(visible) {
    this.currentImmersion?.handleVisualsCCWindingNumbers(visible);
  }
  handleVisualsImmersionDPIndices(visible) {
    this.currentImmersion?.handleVisualsImmersionDPIndices(visible);
  }
  handleVisualsImmersionLCBranches(visible) {
    // paperItemVisibility(this.#regularizationLCBranches, visible); // TODO
  }
  handleVisualsImmersionBirkhoffBranches(visible) {
    // paperItemVisibility(this.#regularizationBirkhoffBranches, visible); // TODO
  }

  handleVisualsRegularizationOrigin(visible) {
    paperItemVisibility(this.#regularizationOriginVisualization, visible);
  }
  handleVisualsRegularizationTarget(visible) {
    paperItemVisibility(this.#regularizationTargetVisualization, visible);
  }
  handleVisualsRegularizationTwoCenterE(visible) {
    paperItemVisibility(this.#regularizationTwoCenterEVisualization, visible);
  }
  handleVisualsRegularizationTwoCenterM(visible) {
    paperItemVisibility(this.#regularizationTwoCenterMVisualization, visible);
  }
  handleVisualsRegularizationTwoCenterEMLine(visible) {
    paperItemVisibility(this.#regularizationTwoCenterEMLine, visible);
  }
  handleVisualsRegularizationXPositive(visible) {
    paperItemVisibility(this.#regularizationXPositive, visible);
  }

  handleImmersionUndo() {
    // this. // TODO ALEX
    console.warn('not implemented handleImmersionUndo');
  }
  handleImmersionRedo() {
    // this. // TODO ALEX
    console.warn('not implemented handleImmersionRedo');
  }
  handleImmersionDuplicate() {
    // this. // TODO ALEX
    console.warn('not implemented handleImmersionDuplicate');
  }
  handleImmersionSmooth() {
    // this. // TODO ALEX
    console.warn('not implemented handleImmersionSmooth');
  }
  handleImmersionFlatten() {
    // this. // TODO ALEX
    console.warn('not implemented handleImmersionFlatten');
  }
  handleImmersionSimplify() {
    // this. // TODO ALEX
    console.warn('not implemented handleImmersionSimplify');
  }
  handleImmersionBringForward() {
    this.currentImmersion.bringToFront();
  }
  handleImmersionSendBack() {
    this.currentImmersion.sendToBack();
  }
  handleImmersionDelete() {
    this.#deleteCurrentImmersion();
  }
  handleCanvasReset() {
    this.resetCanvas();
  }
  handleCanvasExportSvg() {
    // this. // TODO ALEX
    console.warn('not implemented handleCanvasExportSvg');
  }
  handleCanvasImportSvg() {
    // this. // TODO ALEX
    console.warn('not implemented handleCanvasImportSvg');
  }


  // propagating stuff

  /**
   * @param {VisualsView} visualsView
   * @returns {boolean}
   */
  getVisualsImmersionCheckboxChecked(visualsView) {
    return this.#gui.getVisualsImmersionCheckboxChecked(visualsView);
  }
}

export default MyDrawingView;
