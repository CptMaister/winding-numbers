import MySelfIntersection from "./mySelfIntersection.js";


class MyPathSegment {
  segment; // paper.Segment <- see http://paperjs.org/reference/segment/
  /**
   * @type {MySelfIntersection}
   */
  segmentOriginObject; // MySelfIntersection, at least usually, but can be anything that supplied the information for creating the segment

  /**
   * @param {paper.Segment} segment
   * @param {any} segmentOriginObject;
   */
  constructor(segment, segmentOriginObject) {
    this.segment = segment;
    this.segmentOriginObject = segmentOriginObject;
  }

  /**
   * @returns {paper.CurveLocation}
   */
  get curveLocation() {
    return this.segment.location;
  }

  // TODO ALEX 123 Error0.1 see screenshot 2024-10-08 ("this.segment is null")
  static sort(mySegment1, mySegment2) {
    return mySegment1.curveLocation.offset - mySegment2.curveLocation.offset;
  }
}

export default MyPathSegment;
