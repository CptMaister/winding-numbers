import MyDrawingView from "./myDrawingView.js";
import MyImmersion from "./myImmersion.js";

import { ImmersionInfo } from "../enums/immersionInfo.js";
import { Actions, ActionsAvailability } from "../enums/actions.js";
import { MouseMode, MouseModesAvailability } from "../enums/mouseMode.js";
import { VisualsImmersion, VisualsView } from "../enums/visuals.js";
import { Generate, GenerateAvailability } from "../enums/generate.js";

import { getMapValuesAsArray } from "../helpers/basic.js";

const UI_NOT_IMPLEMENTED = [
  MouseMode.IMMERSION_RESIZE,
  MouseMode.IMMERSION_EDIT_SEGMENTS,
  MouseMode.IMMERSION_EDIT_HANDLES,

  MouseMode.VIEW_ZOOM,
  MouseMode.PLACE_ORIGIN,
  MouseMode.PLACE_TARGET,
  MouseMode.PLACE_TWO_CENTER_E,
  MouseMode.PLACE_TWO_CENTER_M,

  Actions.IMMERSION_DUPLICATE,
  Actions.IMMERSION_SMOOTH,
  Actions.IMMERSION_FLATTEN,
  Actions.IMMERSION_SIMPLIFY,
  Actions.IMMERSION_UNDO,
  Actions.IMMERSION_REDO,
  Actions.CANVAS_EXPORT_SVG,
  Actions.CANVAS_IMPORT_SVG,

  VisualsImmersion.PATH_PARTS,

  VisualsImmersion.PATH_PARTS,
  VisualsImmersion.PATH_ORIENTATION,
  VisualsImmersion.DOUBLE_POINT_INDICES,
  VisualsImmersion.BRANCHES_LEVI_CIVITA,
  VisualsImmersion.BRANCHES_BIRKHOFF,

  Generate.ROTATED_KEPLER_ELLIPSE,
  Generate.LEVI_CIVITA_REGULARIZATION,
  Generate.BIRKHOFF_REGULARIZATION,
]

const UNIMPLEMENTED_UI_INVISIBLE = true;

class MyGUI {
  /**
   * @type {MyDrawingView}
   */
  #canvasView; // MyDrawingView

  /**
   * @returns {Map<ImmersionInfo, HTMLSpanElement>}
   */
  #immersionInfoSpans;

  // controls
  /**
   * @returns {Map<MouseMode, HTMLInputElement>}
   */
  #mouseModeRadios;
  /**
   * @returns {Map<Generate, HTMLInputElement>}
   */
  #generateRadios;

  /**
   * @returns {Map<Actions, HTMLButtonElement>}
   */
  #actionsButtons;

  /**
   * @type {Map<VisualsImmersion, boolean>}
   */
  #visualsImmersionBools;
  /**
   * @type {Map<VisualsImmersion, HTMLInputElement>}
   */
  #visualsImmersionCheckboxes;

  /**
   * @type {Map<VisualsView, HTMLInputElement>}
   */
  #visualsViewCheckboxes;

  /**
   * @param {MouseMode} defaultMouseMode
   */
  constructor(defaultMouseMode) {
    this.#initializeImmersionInfoSpans();
    this.#initializeMouseModeRadios(defaultMouseMode);
    this.#initializeActionsButtons();
    this.#initializeVisualsImmersionBoolsAndCheckboxes();
    this.#initializeVisualsViewCheckboxes();
    this.#initializeGenerateRadios();

    this.handleNoImmersionSelected();
  }

  #initializeImmersionInfoSpans() {
    this.#immersionInfoSpans = new Map();
    Object.values(ImmersionInfo).map(immInfo => {
      const htmlSpan = document.getElementById('immersion-analysis-' + immInfo.symbol.description);
      htmlSpan.myImmersionInfoHandler = immInfo.handler;
      this.#immersionInfoSpans.set(immInfo, htmlSpan);
    });
  }

  /**
   * @param {MouseMode} defaultMouseMode
   */
  #initializeMouseModeRadios(defaultMouseMode) {
    this.#mouseModeRadios = new Map();
    Object.values(MouseMode).map(mouseMode => {
      const htmlRadio = document.getElementById('tools-mouse-' + mouseMode.description);
      htmlRadio.myMouseMode = mouseMode;
      htmlRadio.addEventListener('click', this.handleChangeMouseMode.bind(this));
      this.#mouseModeRadios.set(mouseMode, htmlRadio);
    });
    this.handleMouseRadioChecked(defaultMouseMode);
  }

  #initializeGenerateRadios() {
    this.#generateRadios = new Map();
    Object.values(Generate).map(generate => {
      const htmlRadio = document.getElementById('generate-' + generate.description);
      htmlRadio.myGenerate = generate;
      htmlRadio.addEventListener('click', this.handleChangeGenerate.bind(this));
      this.#generateRadios.set(generate, htmlRadio);
    });
    this.handleGenerateRadioChecked(Generate.NOTHING);
  }

  #initializeActionsButtons() {
    this.#actionsButtons = new Map();
    Object.values(Actions).map(action => {
      const htmlButton = document.getElementById('tools-actions-' + action.symbol.description);
      htmlButton.myAction = action;
      htmlButton.addEventListener('click', () => action.handler(this.#canvasView));
      this.#actionsButtons.set(action.symbol, htmlButton);
    });
  }

  #initializeVisualsImmersionBoolsAndCheckboxes() {
    this.#visualsImmersionBools = new Map();
    Object.values(VisualsImmersion).map(visualsImmersion => {
      this.#visualsImmersionBools.set(visualsImmersion.symbol, visualsImmersion.default);
    })

    this.#visualsImmersionCheckboxes = new Map();
    Object.values(VisualsImmersion).map(visualsImmersion => {
      const htmlCheckbox = document.getElementById('visuals-immersion-' + visualsImmersion.symbol.description);
      // htmlCheckbox.checked = visualsImmersion.default; // made obsolete by #setVisualsImmersionCheckboxValues
      htmlCheckbox.myVisualsImmersion = visualsImmersion;
      htmlCheckbox.addEventListener('click', this.handleToggleVisualsImmersion.bind(this));
      this.#visualsImmersionCheckboxes.set(visualsImmersion.symbol, htmlCheckbox);
    });
  }

  #initializeVisualsViewCheckboxes() {
    this.#visualsViewCheckboxes = new Map();
    Object.values(VisualsView).map(visualsView => {
      const htmlCheckbox = document.getElementById('visuals-view-' + visualsView.symbol.description);
      htmlCheckbox.checked = visualsView.default;
      htmlCheckbox.myVisualsView = visualsView;
      htmlCheckbox.addEventListener('click', this.handleToggleVisualsView.bind(this));
      this.#visualsViewCheckboxes.set(visualsView.symbol, htmlCheckbox);
    });
  }

  // some handlers

  handleChangeMouseMode(event) {
    this.#canvasView.handleChangeMouseMode(event.currentTarget.myMouseMode);
  }

  handleChangeGenerate(event) {
    this.#canvasView.handleChangeGenerate(event.currentTarget.myGenerate);
  }

  handleToggleVisualsImmersion(event) {
    const visualsImmersion = event.currentTarget.myVisualsImmersion;
    const newBool = event.currentTarget.checked;

    const currentImmersion = this.#canvasView.currentImmersion;

    if (currentImmersion === undefined) {
      // TODO ALEX CONTINUE
    } else {
      visualsImmersion.handler(this.#canvasView, newBool);
    }
  }

  handleToggleVisualsView(event) {
    const visualsView = event.currentTarget.myVisualsView;
    const newBool = event.currentTarget.checked;

    visualsView.handler(this.#canvasView, newBool);
  }


  /**
   * @param {MyDrawingView} view
   */
  set canvasView(view) {
    this.#canvasView = view;
  }


  // state getters

  /**
   * @param {VisualsView} visualsView
   * @returns {boolean}
   */
  getVisualsImmersionCheckboxChecked(visualsView) {
    return this.#visualsImmersionCheckboxes.get(visualsView.symbol).checked;
  }

  /**
   * @returns {Map<VisualsImmersion, boolean>}
   */
  get visualsImmersionBools() {
    return this.#visualsImmersionBools;
  }


  // ui update handlers

  /**
   * @param {MyImmersion} selectedImmersion
   */
  handleImmersionSelected(selectedImmersion) {
    this.#updateImmersionInfo(selectedImmersion);
    this.#enableDisableUIControls(true);
    this.#setVisualsImmersionCheckboxValues(selectedImmersion.visualsImmersionBools);
  }

  handleNoImmersionSelected() {
    this.#updateImmersionInfo(undefined);
    this.#enableDisableUIControls(false);
    this.#setVisualsImmersionCheckboxValues(this.#visualsImmersionBools);
  }

  /**
   * @param {MyImmersion} immersion
   */
  #updateImmersionInfo(immersion) {
    const noInfo = (immersion === undefined || !immersion.closed);
    getMapValuesAsArray(this.#immersionInfoSpans).map(htmlSpan => {
      const handler = htmlSpan.myImmersionInfoHandler;
      htmlSpan.innerText = (
        noInfo ?
        "--" :
        handler(immersion)
      );
    });

    if (immersion !== undefined && !immersion.closed) {
      this.#immersionInfoSpans.get(ImmersionInfo.CLOSED).innerText = "false";
    }
  }

  /**
   * @param {boolean} anyImmersionSelected
   */
  #enableDisableUIControls(anyImmersionSelected) {
    const immersionsExist = this.#canvasView?.immersionsCount > 0;

    getMapValuesAsArray(this.#mouseModeRadios).map(htmlRadio => {
      const mode = htmlRadio.myMouseMode;
      const enabled = this.#helperCheckEnabled(mode, MouseModesAvailability, immersionsExist, anyImmersionSelected);
      htmlRadio.disabled = !enabled;
      if (this.#helperCheckInvisible(mode)) {
        htmlRadio.parentElement.parentElement.hidden = true;
      }
    });

    getMapValuesAsArray(this.#generateRadios).map(htmlRadio => {
      const generate = htmlRadio.myGenerate;
      const enabled = this.#helperCheckEnabled(generate, GenerateAvailability, immersionsExist, anyImmersionSelected);
      htmlRadio.disabled = !enabled;
      if (this.#helperCheckInvisible(generate)) {
        htmlRadio.parentElement.parentElement.hidden = true;
      }
    });

    getMapValuesAsArray(this.#actionsButtons).map(htmlButton => {
      const action = htmlButton.myAction;
      const enabled = this.#helperCheckEnabled(action, ActionsAvailability, immersionsExist, anyImmersionSelected);
      htmlButton.disabled = !enabled;
      if (this.#helperCheckInvisible(action)) {
        htmlButton.parentElement.parentElement.hidden = true;
      }
    });

    getMapValuesAsArray(this.#visualsImmersionCheckboxes).map(htmlCheckbox => {
      const visuals = htmlCheckbox.myVisualsImmersion;
      const enabled = !UI_NOT_IMPLEMENTED.includes(visuals)
      htmlCheckbox.disabled = !enabled;
      if (this.#helperCheckInvisible(visuals)) {
        htmlCheckbox.parentElement.parentElement.hidden = true;
      }
    });
    getMapValuesAsArray(this.#visualsViewCheckboxes).map(htmlCheckbox => {
      const visuals = htmlCheckbox.myVisualsView;
      const enabled = !UI_NOT_IMPLEMENTED.includes(visuals)
      htmlCheckbox.disabled = !enabled;
      if (this.#helperCheckInvisible(visuals)) {
        htmlCheckbox.parentElement.parentElement.hidden = true;
      }
    });
  }
  #helperCheckEnabled(enumObject, availabilityObject, immersionsExist, anyImmersionSelected) {
    return !UI_NOT_IMPLEMENTED.includes(enumObject)
    && (
      availabilityObject.ALWAYS.includes(enumObject)
      || (immersionsExist && availabilityObject.IMMERSIONS_EXIST.includes(enumObject))
      || (anyImmersionSelected && availabilityObject.IMMERSION_SELECTED.includes(enumObject))
    )
  }
  #helperCheckInvisible(enumObject) {
    return UI_NOT_IMPLEMENTED.includes(enumObject) && UNIMPLEMENTED_UI_INVISIBLE;
  }

  #setVisualsImmersionCheckboxValues(visualsImmersionBools) {
    getMapValuesAsArray(this.#visualsImmersionCheckboxes).map(htmlCheckbox => {
      const value = visualsImmersionBools.get(htmlCheckbox.myVisualsImmersion.symbol);
      if (value === undefined) {
        console.error('setVisualsImmersionCheckboxValues: value undefined for ' + htmlCheckbox.myVisualsImmersion.symbol.description)
      }
      htmlCheckbox.checked = value;
    });
  }

  /**
   * @param {MouseMode} mouseMode
   */
  handleMouseRadioChecked(mouseMode) {
    this.#mouseModeRadios.get(mouseMode).checked = true;
  }
  /**
   * @param {Generate} generate
   */
  handleGenerateRadioChecked(generate) {
    this.#generateRadios.get(generate).checked = true;
  }
}

export default MyGUI;
