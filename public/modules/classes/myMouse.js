import MyDrawingView from "./myDrawingView.js";
import MyImmersion from "./myImmersion.js";
import MySettings from "./mySettings.js";

import { MouseMode } from "../enums/mouseMode.js";

import {
  getMouseEventDelta,
} from "../helpers/paper.js"


class MyMouse {
  /**
   * @type {MouseMode}
   */
  #mode; // MouseMode
  #drawTool; // paper.Tool
  /**
   * @type {MyDrawingView}
   */
  #parentDrawingView; // MyDrawingView

  /**
   * @type {MyImmersion}
   */
  #currentlyDrawnImmersion; // MyImmersion

  /**
   * @param {MyDrawingView} parentDV
   */
  constructor(parentDV) {
    this.#parentDrawingView = parentDV;
    this.mode = this.mySettings.DEFAULT_SETTINGS.mouseMode;
    this.#drawTool = new paper.Tool();
    this.#drawTool.minDistance = this.mySettings.DRAWING_MIN_DISTANCE;

    this.#addEventHandlers();
  }

  #addEventHandlers() {
    this.#drawTool.onMouseMove = (e) => this.#onMouseMove(e, this.#parentDrawingView);
    this.#drawTool.onMouseDown = (e) => this.#onMouseDown(e, this.#parentDrawingView);
    this.#drawTool.onMouseDrag = (e) => this.#onMouseDrag(e, this.#parentDrawingView);
    this.#drawTool.onMouseUp = (e) => this.#onMouseUp(e, this.#parentDrawingView);
  }

  get mode() {
    return this.#mode;
  }

  /**
   * @param {MouseMode} newMode
   */
  set mode(newMode) {
    this.#parentDrawingView.removeCanvasMouseModeCss(this.#mode)
    this.#mode = newMode;
    this.#parentDrawingView.addCanvasMouseModeCss(this.#mode);
    this.#parentDrawingView.handleMouseRadioChecked(this.#mode);
  }

  /**
   * @returns {MySettings}
   */
  get mySettings() {
    return this.#parentDrawingView.settings;
  }

  // TODO ALEX CONTINUE zoom with scroll wheel


  /**
   * @param {paper.MouseEvent} event
   * @param {MyDrawingView} parentView
   */
  #onMouseMove(event, parentView) {
    switch (this.mode) {
      case MouseMode.IMMERSION_SELECT:
        // console.log("onMouseMove handleImmersionSelectionCandidatesMode")
        parentView.handleImmersionSelectionCandidatesMode(true);
        const immersion = this.#handleGetMouseEventImmersion(event, parentView);

        if (immersion !== undefined) {
          immersion.lessVisible = false;
          immersion.accentuated = true;
        }
        break;
      default:
        break;
    }
  }

  /**
   * @param {paper.MouseEvent} event
   * @param {MyDrawingView} parentView
   */
  #onMouseDown(event, parentView) {
    switch (this.mode) {
      case MouseMode.IMMERSION_NEW:
        this.#startDrawingImmersion(event.point, parentView);
        break;
      case MouseMode.IMMERSION_DRAG:
        this.#parentDrawingView.removeCanvasMouseModeCss(MouseMode.IMMERSION_DRAG, false);
        this.#parentDrawingView.addCanvasMouseModeCss(MouseMode.IMMERSION_DRAG, true);
        break;
      case MouseMode.IMMERSION_SELECT:
        this.#handleSelectionClick(event, parentView);
        break;
      default:
        break;
    }
  }

  /**
   * @param {paper.MouseEvent} event
   * @param {MyDrawingView} parentView
   */
  #handleSelectionClick(event, parentView) {
    parentView.currentImmersion = this.#handleGetMouseEventImmersion(event, parentView);
    parentView.handleImmersionSelectionCandidatesMode(true);
  }
  /**
   * @param {paper.MouseEvent} event
   * @param {MyDrawingView} parentView
   */
  #handleGetMouseEventImmersion(event, parentView) {
    if (event.item !== null) {
      const item = event.item;
      if (item.className === "Group") {
        const groupId = item.id;
        const immersion = parentView.findImmersionByGroupId(groupId);
        return immersion;
      }
    }
    return undefined;
  }

  /**
   * @param {paper.MouseEvent} event
   * @param {MyDrawingView} parentView
   */
  #onMouseDrag(event, parentView) {
    if (this.#currentlyDrawnImmersion === undefined) {
      return;
    }

    switch (this.mode) {
      case MouseMode.IMMERSION_NEW:
        if (this.#currentlyDrawnImmersion.closed === false) {
          this.#addPoint(event.point);
        };
        break;
      case MouseMode.IMMERSION_DRAG:
        this.#parentDrawingView.moveCurrentImmersion(getMouseEventDelta(event))
        break;
      case MouseMode.VIEW_PAN:
        this.#parentDrawingView.moveView(getMouseEventDelta(event))
        break;
      default:
        break;
    }
  }

  /**
   * @param {paper.MouseEvent} event
   * @param {MyDrawingView} parentView
   */
  #onMouseUp(event, parentView) {
    switch (this.mode) {
      case MouseMode.IMMERSION_NEW:
        const currentImm = parentView.currentImmersion;
        if (currentImm.closed === false) {
          const success = currentImm.finishDrawing(true, true, true);
          if (success) {
            this.#parentDrawingView.handleImmersionSelectedOrFinished();
            this.mode = MouseMode.IMMERSION_DRAG;
          } else { 
            // parentView.resetCanvas(); // why did I add this before???
          }
        }
        break;
      case MouseMode.IMMERSION_DRAG:
        this.#parentDrawingView.removeCanvasMouseModeCss(MouseMode.IMMERSION_DRAG, true);
        this.#parentDrawingView.addCanvasMouseModeCss(MouseMode.IMMERSION_DRAG, false);
        break;
      default:
        break;
    }
  }


  /**
   * @param {paper.Point} point
   * @param {MyDrawingView} parentView
   */
  #startDrawingImmersion(point, parentView) {
    this.#currentlyDrawnImmersion = parentView.createNewImmersion(point);
  }

  /**
   * @param {paper.Point} point
   */
  #addPoint(point) {
    this.#currentlyDrawnImmersion.addPathPoint(point);
  }
}

export default MyMouse;
