import MyImmersion from "./myImmersion.js";
import MyImmersionPart from "./myImmersionPart.js";
import MySelfIntersection from "./mySelfIntersection.js";
import MyOrientedCCImmersionPart from "./myOrientedCCImmersionPart.js";

import {
  joinPaths,
  closePathWithoutGap,
  calculateWindingNumberForAreaAndPath,
} from "../helpers/paper.js"


class MyConnectedComponent {
  /**
   * @type {MyImmersion}
   */
  parentImmersion;
  /**
   * @type {MyOrientedCCImmersionPart[]}
   */
  orientedImmersionParts;
  /**
   * @type {boolean}
   */
  isUnboundedComponent;

  /**
   * @type {String}
   */
  debuggingName;

  // references to mathematical companions
  /**
   * @type {MySelfIntersection[]}
   */
  boundaryVertices; // possibly counting a vertex twice, if it appears multiple times in the boundary (for example for a circle with an inner loop (standard curve K2)). Should be equal to amount of orientedImmersionParts, if there are any vertices, else the boundary is a circle and has no vertices.

  /**
   * @type {paper.Path}
   */
  boundaryPath;

  // calculated stuff
  /**
   * @type {integer}
   */
  #windingNumber;

  /**
   * @param {MyImmersion} parentImm
   * @param {MyOrientedCCImmersionPart[]} orientedParts expected to be sorted properly, so that concatenating the paths (adjusted with their orientation) gives the boundaryPath of the connected component.
   * @param {boolean} isUnboundedComponent
   */
  constructor(parentImm, orientedParts, debuggingName="", isUnboundedComponent=false) {
    this.parentImmersion = parentImm;
    this.orientedImmersionParts = orientedParts;
    this.isUnboundedComponent = isUnboundedComponent;

    this.debuggingName = debuggingName;

    this.#additionalInitialization();
  }

  destroySelf() {
    // nothing to do here
  }

  /**
   * @returns {Integer}
   */
  get windingNumber() {
    if (this.#windingNumber !== undefined) {
      return this.#windingNumber;
    }
    this.#windingNumber = this.calculateWindingNumber();
    return this.#windingNumber;
  }

  /**
   * @returns {MyImmersionPart[]}
   */
  get originalImmersionParts() {
    return this.orientedImmersionParts.map(oip => oip.originalImmersionPart)
  }

  #additionalInitialization() {
    this.#initializeBoundaryVertices();
    this.#setVerticesConnectedComponent();
    this.#setImmersionPartsConnectedComponent();
    this.#initializeBoundaryPath();
  }

  #initializeBoundaryVertices() {
    this.boundaryVertices = this.orientedImmersionParts
      .map(orientedImmPart => orientedImmPart.adjustedStartPoint)
      .filter(vertex => vertex !== null);
  }

  #setVerticesConnectedComponent() {
    this.boundaryVertices.map(vertex => {
      vertex.addConnectedComponent(this);
    });
  }

  #setImmersionPartsConnectedComponent() {
    this.orientedImmersionParts.map(orientedPart =>
      orientedPart.connectedComponent = this);
  }

  #initializeBoundaryPath() {
    this.boundaryPath = this.orientedImmersionParts[0].adjustedPartPath.clone();

    if (this.orientedImmersionParts.length > 1) {
      this.orientedImmersionParts.map((orientedPart, index) => {
        if (index > 0) {
          const nextPartPath = orientedPart.adjustedPartPath;
          joinPaths(this.boundaryPath, nextPartPath, this.parentImmersion.parentViewSettings.EPSILON_JOIN_POINTS_TOLERANCE);
        }
      })

      closePathWithoutGap(this.boundaryPath, this.parentImmersion.parentViewSettings.EPSILON_JOIN_POINTS_TOLERANCE);

      if (!this.boundaryPath.closed) {
        console.warn('WARNING: initializeBoundaryPath: segments joined are not closed component, at connected component path parts joining')
      }
    }

    this.parentImmersion.addVisibleElement(this.boundaryPath);
  }

  /**
   * @returns {MyImmersionPart[]}
   */
  getAllOtherOriginalImmersionParts() {
    const ownParts = this.originalImmersionParts;
    const otherParts = this.parentImmersion.immersionParts
      .filter(part => !ownParts.includes(part));
    return otherParts;
  }

  /**
   * @returns {integer}
   */
  calculateWindingNumber() {
    if (this.isUnboundedComponent) {
      return 0;
    } else {
      const wholeImmersionPath = this.parentImmersion.pathClone;
      return calculateWindingNumberForAreaAndPath(this.boundaryPath, wholeImmersionPath);
    }
  }
}

export default MyConnectedComponent;
