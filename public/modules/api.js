import MyImmersion from "./classes/myImmersion.js";
import { DEBUGGING_TEST_CURVES_DESTROY_EVERYTHING } from "./constants/debug.js";

import { addMyPoints } from "./helpers/paper.js";


///*** External-API-like stuff ***///

/**
 * Created for testing the mathematical soundness of the results the codes calculations for some example curves. Might later be used for just inserting a curve and then
 * @param {{ x: number, y: number }[]} exampleCurvePoints array of points which are just objects with an .x and .y value each, which describes each point in cartesian coordinates. The ordering of the points will be used for constructing the curve. At the end the curve will always be closed.
 * @param {{ x: number, y: number }} shiftCurvePosition shifts the curves points' coordinates so that one can more easily check all of them by eye. If not undefined, the curves will be visible.
 * @param {{ x: number, y: number }} origin only necessary for j1 and j2
 * @returns
 */
function analyzeExampleCurve(
  exampleCurvePoints,
  shiftCurvePosition=undefined,
  origin=undefined,
  destroyAfterAnalysis=true
) {
  const shiftPositions = shiftCurvePosition ?? { x: 0, y: 0 };
  const curvePoints = exampleCurvePoints
    .map(point => addMyPoints(point, shiftPositions));
  const analyzeImmersion = new MyImmersion(undefined, curvePoints, shiftCurvePosition !== undefined);
  analyzeImmersion.finishDrawing(false, false, true);

  let analysisResults;
  if (analyzeImmersion.noTangentialCrossings === false) {
    analysisResults = {
      noTangentialCrossings: analyzeImmersion.noTangentialCrossings,
    }
  } else {
    analysisResults = {
      noTangentialCrossings: analyzeImmersion.noTangentialCrossings,
      crossingPoints: analyzeImmersion.selfIntersections
        .map(selfInt => ({ x: selfInt.point.x, y: selfInt.point.y }))
        .map(point => addMyPoints(point, shiftPositions)),
      immersionPartsCount: analyzeImmersion.immersionParts.length,
      connectedComponentsCount: analyzeImmersion.connectedComponents.length,
      connectedComponentWindingNumbers: analyzeImmersion?.connectedComponentWindingNumbers,
      doublePointIndices: analyzeImmersion?.doublePointIndices,
      jPlus: analyzeImmersion?.jPlus,
    }
  }

  if (destroyAfterAnalysis) {
    analyzeImmersion.destroySelf()
  }
  return analysisResults;
}


export { analyzeExampleCurve };
