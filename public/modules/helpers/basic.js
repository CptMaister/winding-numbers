
/**
 * Spreads out the scaling function over the specified duration, so that the this jumps less in the eyes of the user.
 * @param {(factor: number) => void} scalingFunction
 * @param {number} scaleFactor
 * @param {int} durationMilli in milliseconds
 */
function smoothScaling(scalingFunction, scaleFactor, durationMilli) {
  const frameCount = Math.ceil(durationMilli / 20); // this results in at least 50 frames per second, which is enough
  const singleFrameDuration = durationMilli / frameCount;
  const scalePerFrame = 1 + ((scaleFactor-1) / frameCount);
  for (let i = 0; i < frameCount; i++) {
    setTimeout(
      () => scalingFunction(scalePerFrame),
      i * singleFrameDuration
    );
  }
}

/**
 * Normal slice for startInclusive <= endExclusive, but else with special wrapping behaviour.
 * @param {Object[]} array
 * @param {int} startInclusive
 * @param {int} endExclusive
 * @returns
 */
function wrappingSlice(array, startInclusive, endExclusive) {
  const startAdjusted = properMod(startInclusive, array.length);
  const endAdjusted = properMod(endExclusive, array.length);

  if (startAdjusted <= endAdjusted) {
    return array.slice(startAdjusted, endAdjusted);
  } else {
    return [
      ...array.slice(startAdjusted, array.length),
      ...array.slice(0, endAdjusted)
    ]
  }
}

/**
 * Returns the smallest non-negative result that one can reasonably expect from modulus operation. More precisely, returns the smallest non-negative representative of the resulting equivalence class.
 */
function properMod(number, modulus) {
  return ((number % modulus) + modulus) % modulus;
}

/**
 * @param {number} rNormed between 0 and 1
 * @param {number} gNormed between 0 and 1
 * @param {number} bNormed between 0 and 1
 * @returns {string}
 */
function rgbToHex(rNormed, gNormed, bNormed) {
  return `#${componentToHex(rNormed)}${componentToHex(gNormed)}${componentToHex(bNormed)}`
}
function componentToHex(valueNormed) {
  const valueHex = Math.floor(255 * valueNormed).toString(16);
  return (valueHex.length === 1 ? '0' : '') + valueHex;
}

/**
 * Taken from https://stackoverflow.com/a/16436975
 *
 * Then added functionality to compare objects with .x and .y values.
 */
function arraysEqualXY(a, b, sortArrays=false) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length !== b.length) return false;

  // Please note that calling sort on an array will modify that array.
  // you might want to clone your array first.
  if (sortArrays) {
    a = a.toSorted();
    b = b.toSorted();
  }

  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== undefined && a[i].x !== undefined) {
      if (a[i].x !== b[i].x || a[i].y !== b[i].y) return false;
    } else {
      if (a[i] !== b[i]) return false;
    }
  }
  return true;
}

/**
 * Returns a random element from an array.
 */
function randomArrayElement(array) {
  return array[Math.floor(Math.random() * array.length)];
}

/**
 * Returns a copy of the input array which only contains unique elements.
 */
function arrayUniqueElements(array) {
  return [...new Set(array)];
}

/**
 * @param {T[]} array
 * @returns {T | undefined} the last element of the array, else undefined
 */
function last(array) {
  if (array.length > 0) {
    return array[array.length - 1];
  } else {
    return undefined;
  }
}

/**
 * @param {Map<any, T>} map
 * @returns {T[]} all map contents (values) as an array
 */
function getMapValuesAsArray(map) {
  return Array.from(map.values());
}

/**
 * Returns number if it is within the bounds of min and max, else whatever bound it breaks.
 * @param {number} number
 * @param {number} min
 * @param {number} max
 * @returns {number}
 */
function clamp(number, min, max) {
  return Math.min(Math.max(number, min), max);
}

/**
 * Like in python, returns an array consisting of all the numbers starting from start (inclusive) up to end (exclusive), with gaps of step.
 * @param {integer} start
 * @param {integer} end
 * @param {integer} step
 * @returns {integer[]}
 */
function range(start, end, step) {
  const resultArray = [];
  const stepSafe = (step == undefined ? 1 : step);
  for (let i = start; i < end; i = i+stepSafe) {
    resultArray.push(i);
  }
  return resultArray;
}

/**
 * Creates an array consisting of the element inside it amount times.
 * @param {any} element
 * @param {integer} amount
 * @returns {any[]}
 */
function copies(element, amount) {
  const resultArray = [];
  for (let i = 0; i < amount; i++) {
    resultArray.push(element);
  }
  return resultArray;
}

/**
 * @param {T[]} array
 * @param {integer} shiftDistance
 * @returns {T[]} with all entries shifted by shiftDistance, wrapping around
 */
function shiftArrayEntries(array, shiftDistance) { // shiftDistance can be negative
  return range(0, array.length).map(index =>
    array[properMod(index - shiftDistance, array.length)]
  )
}

/**
 * Returns a bouncing modulus, i.e for modulus 1 this returns 0, 0.2, 0.4, 0.6, 0.8, 1.0, 0.8, 0.6, 0.4, 0.2, 0, 0.2, ...
 * @param {number} number
 * @param {number} modulus
 * @returns
 */
function bounceMod(number, modulus) {
  const mod = properMod(number, 2 * modulus);
  return (
    mod > modulus ?
    2 * modulus - mod :
    mod
  )
}

function isArrayOfType(array, type) {
  return (
    Array.isArray(array)
    && array.every(el => (typeof el) === type)
  )
}

function getArrayNextEntry(array, current) {
  const currentIndex = array.indexOf(current);
  const index = (currentIndex === -1) ? 0 : currentIndex;
  const next = array[(index + 1) % array.length];
  return next;
}

function degreeToRadians(degree) {
  return 2 * Math.PI * (degree / 360)
}


function sortNumbersAscending(a, b, getter=((c) => c)) {
  return getter(a) - getter(b);
}
function sortLengthsAscending(a, b) {
  return sortNumbersAscending(a, b, (c) => c.length);
}







export {
  smoothScaling,
  wrappingSlice,
  properMod,
  rgbToHex,
  componentToHex,
  arraysEqualXY,
  randomArrayElement,
  arrayUniqueElements,
  last,
  getMapValuesAsArray,

  clamp,
  range,
  copies,
  shiftArrayEntries,
  bounceMod,
  isArrayOfType,
  getArrayNextEntry,
  degreeToRadians,
  sortNumbersAscending,
  sortLengthsAscending
};
