
///*** Mathematical Formulas ***///


///*** Invariant Formulas ***///

/**
 * Using Viro's Formula to calculate the J-plus-invariant of an immersion. This function takes only the information necessary for the calculation, so the actual immersion is not needed.
 *
 * See page 12 of https://arxiv.org/abs/2210.00871 for more information.
 *
 * @param {integer[]} connectedComponentWindingNumbers
 * @param {integer[]} doublePointIndices
 * @returns {integer}
 */
function virosFormulaJPlus(connectedComponentWindingNumbers, doublePointIndices) {
  const squaringProcedure = (number) => (number * number);
  // const squaringProcedure = (number) => (Math.pow(number, 2)); // according to the internet, this is slower than just doing number*number.

  const n_k = doublePointIndices.length;
  const sum_squared_cc_windingNumbers = connectedComponentWindingNumbers
    .map(squaringProcedure)
    .reduce((a, b) => a + b, 0);
  const sum_squared_dp_indices = doublePointIndices
    .map(squaringProcedure)
    .reduce((a, b) => a + b, 0);
  const calculatedJPlus = 1 + n_k - sum_squared_cc_windingNumbers + sum_squared_dp_indices;
  return calculatedJPlus;
}

/**
 * Calculate the rotation number (also known as rotation index) of an immersion, using only the winding numbers of the connected components and the double point indices. This function takes only the information necessary for the calculation, so the actual immersion is not needed.
 *
 * See page 17 of https://arxiv.org/abs/2210.00871 for more information.
 *
 * @param {integer[]} connectedComponentWindingNumbers
 * @param {integer[]} doublePointIndices
 * @returns {integer}
 */
function calculateRotationNumber(connectedComponentWindingNumbers, doublePointIndices) {
  const sum_cc_windingNumbers = connectedComponentWindingNumbers
    .reduce((a, b) => a + b, 0);
  const sum_dp_indices = doublePointIndices
    .reduce((a, b) => a + b, 0);
  const calculatedRoationNumber = sum_cc_windingNumbers - sum_dp_indices;
  return calculatedRoationNumber;
}


///*** Bifurcation Formulas ***///


/**
 * Calculates the lower bound of the J⁺-Invariant for an immersion after a bifurcation. This lower bound is attained if the number of double points after the bifurcation is as small as possible.
 *
 * See page 17 of https://arxiv.org/abs/2210.02968 for more information.
 *
 * @param {integer} previousJPlus
 * @param {integer} bifurcationNumber
 * @returns {integer}
 */
function bifurcationJPlusLowerBoundFormula(previousJPlus, bifurcationNumber) {
  const bifNumberSquared = bifurcationNumber * bifurcationNumber;
  const lowerBound = bifNumberSquared * previousJPlus - (bifNumberSquared - bifurcationNumber);
  return lowerBound;
}

/**
 * Calculates the rotation number for an immersion after a bifurcation.
 *
 * See Theorem 2.5, on page 20, of https://arxiv.org/abs/2210.02968 for more information.
 *
 * @param {integer} previousRotationNumber
 * @param {integer} bifurcationNumber
 * @returns {integer}
 */
function bifurcationRotationNumberFormula(previousRotationNumber, bifurcationNumber) {
  return previousRotationNumber * bifurcationNumber;
}


export {
  virosFormulaJPlus,
  calculateRotationNumber,
  bifurcationJPlusLowerBoundFormula,
  bifurcationRotationNumberFormula,
};
