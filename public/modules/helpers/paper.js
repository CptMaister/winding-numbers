import { CANVAS_TEXT_COLOR } from "../classes/myCanvasInfo.js";

import { StartOrEnd } from "../enums/startOrEnd.js";
import { sortLengthsAscending } from "./basic.js";


///*** Procedures on paper-Objects like Point, Path etc ***///

/**
 * @param {paper.MouseEvent} event
 * @returns {{ x: number, y: number }}
 */
function getMouseEventDelta(event) {
  return { x: event.event.movementX, y: event.event.movementY };
}

/**
 * @param {paper.Point} point
 * @param {string} fillColor
 * @param {number} radius non-negative integer
 * @param {boolean} visible
 * @returns {paper.Path.Circle}
 */
function visualizePointWithCircle(point, fillColor='orange', radius=5, visible=true) {
  return new paper.Path.Circle({
    center: point,
    radius: radius,
    fillColor: fillColor,
    visible: visible,
  })
}

/**
 * @param {paper.Point} point
 * @param {number} radius non-negative integer
 * @param {string} color
 * @param {boolean} visible
 * @param {string} text
 * @returns {paper.Group}
 */
function visualizePointWithMarkerBullsEye(
  point,
  radius,
  color,
  visible=true,
  markerText=""
) {
  const innerCircle = new paper.Path.Circle({
    center: point,
    radius: radius * (1/3),
    fillColor: 'white',
    visible: true,
  })
  const middleCircle = new paper.Path.Circle({
    center: point,
    radius: radius * (2/3),
    strokeWidth: radius / 10,
    strokeColor: color,
    visible: true,
  })
  const outerCircle = new paper.Path.Circle({
    center: point,
    radius: radius,
    strokeWidth: radius / 10,
    strokeColor: color,
    visible: true,
  })

  const verticalLine = new paper.Path({
    segments: [
      new paper.Point(point.x, point.y - radius),
      new paper.Point(point.x, point.y + radius)
    ],
    strokeWidth: radius / 10,
    strokeColor: color,
  });
  const horizontalLine = new paper.Path({
    segments: [
      new paper.Point(point.x - radius, point.y),
      new paper.Point(point.x + radius, point.y)
    ],
    strokeWidth: radius / 10,
    strokeColor: color,
  });


  const textItem = new paper.PointText({
    content: markerText,
    point: point,
    fillColor: CANVAS_TEXT_COLOR,
    fontWeight: 'bold',
    fontSize: radius * (2/3),
  });
  paperPointTextMoveCenterToPosition(textItem);

  const itemGroup = new paper.Group();
  itemGroup.addChildren([
    innerCircle, middleCircle, outerCircle,
    verticalLine, horizontalLine,
    textItem,
  ]);

  innerCircle.bringToFront();
  textItem.bringToFront();

  itemGroup.visible=visible;
  return itemGroup;
}

/**
 * @param {paper.Path} path
 * @param {Number} relativeLocation number between 0 and 1
 * @param {String} color
 * @returns {paper.Group}
 */
function visualizePathTangent(path, relativeLocation, color) {
  const pathOffset = path.length * relativeLocation;
  const pathPoint = path.getPointAt(pathOffset);
  const tangentDirectionAsPoint = path.getTangentAt(pathOffset);
  const tangentTipPoint = pathPoint.add(tangentDirectionAsPoint.multiply(50));
  const tangentVisualized = new paper.Path({
    segments: [pathPoint, tangentTipPoint],
    strokeColor: color,
    strokeWidth: 2,
  })
  const tangentTip = visualizePointWithCircle(tangentTipPoint, color, 4)

  const visualizationGroup = new paper.Group();
  visualizationGroup.addChildren([tangentVisualized, tangentTip]);
  return visualizationGroup;
}

/**
 * @param {{ x: number, y: number }} p1
 * @param {{ x: number, y: number }} p2
 * @param {number} epsilon non-negative
 * @returns {boolean}
 */
function pointsEqualsEpsilon(p1, p2, epsilon=0.00001) {
  return (
    Math.abs(p1.x - p2.x) <= epsilon
    && Math.abs(p1.y - p2.y) <= epsilon
  )
}



/**
 * Adds objects' x and y properties. This does not use paper.Point objects.
 * @param {{ x: number, y: number }} p1
 * @param {{ x: number, y: number }} p2
 * @returns {{ x: number, y: number }}
 */
function addMyPoints(p1, p2) {
  return { x: p1.x + p2.x, y: p1.y + p2.y };
}

/**
 * @param {paper.Item} item
 * @param {boolean} visible
 */
function paperItemVisibility(item, visible) {
  item.visible = visible;
  if (visible) {
    item.bringToFront();
  }
}

/**
 * @param {paper.PointText} pointTextItem
 */
function paperPointTextMoveCenterToPosition(pointTextItem) {
  pointTextItem.position.x -= (pointTextItem.bounds.width / 2);
  pointTextItem.position.y += (pointTextItem.bounds.height * (2/7));
}

/**
 * @param {paper.Path} path
 * @param {paper.Point} point
 * @returns {boolean}
 */
function getPathContainsPoint(path, point) {
  return path.contains(point);
}

/**
 * This manipulates the input path.
 * @param {paper.Path} path
 * @param {integer} amount
 */
function removePathTrailingLength(path, length) {
  if (path.length * 2 < length) {
    console.warn("removePathTrailingLength: path has only length " + path.length + ", but trying to remove length " + length + " from both sides. Instead do nothing.")
    return;
  }
  const startCutoffSegment = path.divideAt(length);
  path.removeSegments(0, path.segments.indexOf(startCutoffSegment));
  const endCutoffSegment = path.divideAt(path.length - length);
  path.removeSegments(path.segments.indexOf(endCutoffSegment) + 1, path.segments.length);
}

/**
 * This manipulates the input path.
 * @param {paper.Path} path
 * @param {integer} amount
 */
function removePathTrailingSegments(path, amount=1) {
  for (let i = 0; i < amount; i++) {
    const segmentsCount = path.segments.length;
    if (segmentsCount <= 3) {
      break;
    }
    path.removeSegment(segmentsCount - 1);
    path.removeSegment(0);
  }
}


/**
 * @param {number} gapLength
 * @param {paper.Path} closedPath must be closed
 * @param {paper.Point} gapCenterPoint
 * @returns {paper.Path}
 */
function addGapToPathAtPoint(gapLength, closedPath, gapCenterPoint) {

  // find the offsets at which the gap should start and end
  const locationOfGapCenterOnPath = closedPath.getNearestLocation(gapCenterPoint);
  const offsetGapStart = locationOfGapCenterOnPath.offset - (gapLength / 2);

  closedPath.splitAt(offsetGapStart); // now not closed anymore
  const extraPath = closedPath.splitAt(gapLength);

  // now we need to decide which of the two paths is the shorter one (i.e. the gap), remove that one, and keep the longer one
  const pathsOrderedByLengthAscending = [closedPath, extraPath]
    .sort(sortLengthsAscending);

  const removePath = pathsOrderedByLengthAscending[0];
  const keepPath = pathsOrderedByLengthAscending[1];

  removePath.remove();

  return keepPath;
}

/**
 * This manipulates the input path.
 * @param {paper.Path} path
 * @param {StartOrEnd} startOrEnd
 */
function resetPathDanglingHandles(path, startOrEnd=undefined) {
  if (startOrEnd === undefined || startOrEnd === StartOrEnd.START) {
    path.firstSegment.handleIn = new paper.Point(0, 0);
  }
  if (startOrEnd === undefined || startOrEnd === StartOrEnd.END) {
    path.lastSegment.handleOut = new paper.Point(0, 0);
  }
}

/**
 * Probably unnecessary, but automates getting a point that is properly distanced from the path to avoid rounding errors.
 * @param {paper.Path} path
 * @param {boolean} trustPaperJsToGiveAProperPoint
 * @param {integer} loopWindingNumberPointSearchLimit
 */
function getInnerPointInPathArea(
  path,
  trustPaperJsToGiveAProperPoint=true,
  loopWindingNumberPointSearchLimit=77
) {
  if (trustPaperJsToGiveAProperPoint) {
    return path.interiorPoint;

  } else {
    const sensibleSize = Math.min(path.bounds.x, path.bounds.y) / 64;
  
    let firstCandidate = path.interiorPoint
    let nextCandidate = firstCandidate;
  
    let counter = 0;
    while(counter < loopWindingNumberPointSearchLimit) {
      if (
        !isPointInPathArea(nextCandidate, path)
        || checkPointIsOnPath(nextCandidate, path)
      ) {
        nextCandidate = firstCandidate;
      } else {
        return nextCandidate;
      }
  
      nextCandidate = path.interiorPoint.add(getRandomVectorOfLength(sensibleSize));

      counter++; // this is just to make sure we don't enter an infinite loop for an immersion with a really small connected component
    }
  
    if (counter >= loopWindingNumberPointSearchLimit) {
      console.error('getInnerPointInPathArea: could not find a point suitable for the given tolerance')
    }
  }
}

/**
 * @param {paper.Point} point
 * @param {paper.Path} path
 */
function isPointInPathArea(point, path) {
  return path.contains(point);
}

/**
 * @param {paper.Path} path
 * @param {Number} directionDegrees the direction in which the outer point should lie, as seen from the bounding rectangle's center
 */
function getOuterPointOfPath(path, directionDegrees=undefined) {
  const boundingRect = path.bounds;
  const largeEnoughDistance = boundingRect.width + boundingRect.height;
  const rectCenter = boundingRect.center;
  const pointRightOfRect = rectCenter.add(new paper.Point(largeEnoughDistance, 0));

  if (directionDegrees !== undefined) {
    // rotate returns a new object, does not modify the original Point, see http://paperjs.org/reference/point/#rotate-angle-center
    return pointRightOfRect.rotate(directionDegrees, rectCenter);
  }
  return pointRightOfRect;
}

/**
 * @param {number} length 
 */
function getRandomVectorOfLength(length) {
  const directionDegrees = Math.random() * 360;
  const vector = new paper.Point(length, 0);
  return vector.rotate(directionDegrees)
}

/**
 * @param {paper.Path} path
 * @returns {paper.Point}
 */
function getMiddlePointOfPath(path) {
  return path.getPointAt(path.length / 2);
}

/**
 *
 * @param {paper.Point} p1
 * @param {paper.Point} p2
 * @returns {number} non-negative
 */
function distanceBetweenPoints(p1, p2) {
  if (typeof p1.getDistance === "function") {
    return p1.getDistance(p2);
  } else {
    const xDiff = p1.x - p2.x
    const yDiff = p1.y - p2.y
    return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
  }
}

/**
 * Try to combine the paths by first checking if originPath ends at the same point as addPath (within tolerance of the provided argument), then glueing them together by combining the last in-handle of originPath and the first out-handle of add-Path and then merging the segments together. The originPath gets mutated, but addPath is not changed.
 * @param {paper.Path} originPath gets mutated!
 * @param {paper.Path} addPath does not change.
 * @param {number} epsilonJoinPointsTolerance non-negative
 */
function joinPaths(originPath, addPath, epsilonJoinPointsTolerance) {
  if (!checkPathSegmentsJoinable(originPath, addPath, epsilonJoinPointsTolerance)) {

    visualizePointWithCircle(originPath.lastSegment.point, "purple", 8, true);
    visualizePointWithCircle(addPath.firstSegment.point, "lime", 7, true);
    visualizePointWithCircle(originPath.lastSegment.point, "purple", 6, true);

    originPath.visible = true;
    originPath.strokeWidth = 5;
    originPath.strokeColor = "red";
    addPath.visible = true;
    addPath.strokeWidth = 5;
    addPath.strokeColor = "green";
    throw new Error('joinPaths: checkPathSegmentsJoinable false');
  }

  originPath.lastSegment.handleOut = addPath.firstSegment.handleOut;

  const newSegments = addPath.segments.slice(1);
  originPath.addSegments(newSegments);
}

/**
 * Closes a path, but checks first if the path's start and end point are close enough together.
 * @param {paper.Path} path
 * @param {number} epsilonJoinPointsTolerance
 */
function closePathWithoutGap(path, epsilonJoinPointsTolerance) {
  if (!checkPointsCloseEnough(
    path.firstSegment.point,
    path.lastSegment.point,
    epsilonJoinPointsTolerance
  )) {
    throw new Error('closePathWithoutGap: checkPointsCloseEnough false');
  }

  path.lastSegment.point = path.firstSegment.point;
  resetPathDanglingHandles(path);

  path.closePath();
}

/**
* @param {paper.Path} firstPath
* @param {paper.Path} secondPath
* @param {number} epsilonJoinPointsTolerance non-negative
 */
function checkPathSegmentsJoinable(firstPath, secondPath, epsilonJoinPointsTolerance) {
  return checkPointsCloseEnough(
    firstPath.lastSegment.point,
    secondPath.firstSegment.point,
    epsilonJoinPointsTolerance
  );
}

/**
* @param {paper.Point} p1
* @param {paper.Point} p2
* @param {number} epsilonJoinPointsTolerance non-negative
 */
function checkPointsCloseEnough(p1, p2, epsilonJoinPointsTolerance=0.1) {
  return distanceBetweenPoints(p1, p2) <= epsilonJoinPointsTolerance;
}

/**
 * Returns the rotation number of a path without self-intersections by calculation the winding number for any inner point, which is then equal to the rotation number by definition.
 * @param {paper.Path} path
 * @returns {(-1 | +1)}
 */
function getRotationNumberOfCircleEmbedding(path) {
  checkPathIsCircleEmbedding(path);
  return calculateWindingNumberForAreaAndPath(path, path);
}

/**
 * @param {paper.Path} path
 */
function checkPathIsCircleEmbedding(path) {
  if (path.closed === false) {
    throw new Error('checkPathIsCircleEmbedding: path must be closed!');
  }
  if (checkPathIntersectsSelf(path)) {
    throw new Error('checkPathIsCircleEmbedding: path must have no self-intersections!');
  }
}

/**
 * Throws Error if point lies on the path.
 * @param {paper.Point} point
 * @param {paper.Path} path
 * @param {Number} tolerance
 */
function checkPointIsOnPath(point, path, tolerance=0.00001) {
  const nearestPoint = path.getNearestPoint(point);
  const distance = distanceBetweenPoints(nearestPoint, point);
  return distance <= tolerance;
}

/**
 * @param {paper.Point} point
 * @param {paper.Path} path
 * @param {integer} loopLimit
 * @returns {{linePath: paper.Path, lineCrossings: paper.CurveLocation[]}[]}
 */
function findLineFromPointToOutsideOfPathWithoutTangentialIntersections(point, path, loopLimit = 150) {
  let directionDegrees = 0;
  let intersectionTestLine;
  let loopCounter = 0;

  while (true) {
    if (loopCounter >= loopLimit) {
      throw new Error(`BREAK LOOP findLineFromPointToOutsideOfPathWithoutTangentialIntersections: exceeded loop limit of ${loopLimit}`);
    }
    loopCounter++;

    const outerPoint = getOuterPointOfPath(path, directionDegrees);
    intersectionTestLine = new paper.Path({
      segments: [point, outerPoint],
    });
    intersectionTestLine.visible = false;

    try {
      const crossings = checkOnlyTransversalIntersections(intersectionTestLine, path);
      return {
        linePath: intersectionTestLine,
        lineCrossings: crossings
      }
    } catch (error) {
      directionDegrees = Math.random() * 360;
      intersectionTestLine.remove();
    }
  }
}

/**
 * @param {paper.Path} firstPath
 * @param {paper.Path} otherPath
 * @returns {paper.CurveLocation[]}
 */
function checkOnlyTransversalIntersections(firstPath, otherPath) {
  const crossings = firstPath.getCrossings(otherPath);
  const intersections = firstPath.getIntersections(otherPath);
  if (crossings.length !== intersections.length) {
    throw new Error ('checkOnlyTransversalIntersections: paths intersect tangentially, i.e. not transversally. TODO add perturbation to avoid this case, until then just don\'t do this');
  }

  return crossings;
}

/**
 * @param {paper.Path} path
 * @returns {boolean}
 */
function checkPathIntersectsSelf(path) {
  return path.getIntersections(path).length > 0;
}

/**
 * Create clones for the supplied part paths (expected to not have self-intersections) at the given offset value, using paperjs-offset. If checkPartValidity is true, then the paths in the created array will be destroyed (paper's .remove()) and the return boolean is false.
 * @param {paper.Path[]} partPaths
 * @param {number} offset
 * @param {boolean} checkPartValidity
 * @param {boolean} visible
 * @throws {Error}
 * @returns {[boolean, paper.Path[]]}
 */
function createPartsClonesAtOffset(partPaths, offset, checkPartValidity=true, visible=false) {

  let success = true;

  const offsetParts = [];
  for (const part of partPaths) {
    const offsetPart = PaperOffset.offset(part, offset, {insert: true}); // TODO ALEX CONTINUE: test paper-clipper again, maybe it performs better?
    offsetPart.visible = visible;

    // // TODO ALEX remove
    // offsetPart.visible = true;
    // offsetPart.strokeColor = "black";
    // offsetPart.strokeWidth = 2;

    // console.log({offsetPart})

    offsetParts.push(offsetPart);

    if (checkPartValidity) {
      const offsetPartLength = offsetPart.length;
      const tolerance = offsetPartPathIntersectionCheckFactor * Math.abs(offset);

      const intersections = offsetPart.getIntersections(offsetPart);
      // console.log({intxLength: intersections.length})
      if (
        offsetPart === undefined
        || (
          offsetPart.children !== undefined
          && offsetPart.children.length > 0
        )
        || offsetPart.segments.length === 0
        || intersections.length > 1
        || (
          // we allow intersections at the ends of the curve, for when start and finish are at the same point and the ends happen to slightly overlap. For this, we arbitrarily set a relative value above which we tolerate this, depending on the offset value.
          intersections.length === 1
          && (
            (
              intersections
              .filter(intx => checkPartClonesOffsetHelperIntersectionAwayFromEnds(tolerance, intx, offsetPartLength))
              .length > 0
            ) || part.closed === true
          )
        )
      ) {
        success = false;
        offsetParts.forEach(part => part.remove());
        break;
      }
    }
  }

  return [success, offsetParts];
}
function checkPartClonesOffsetHelperIntersectionAwayFromEnds(tolerance, intersectionLocation, offsetPartLength) {
  const firstCurveLocation = intersectionLocation;
  const secondCurveLocation = intersectionLocation.intersection;

  const result = (
    (
      firstCurveLocation.offset > tolerance
    ) && (
      firstCurveLocation.offset < (offsetPartLength - tolerance)
    ) && (
      secondCurveLocation.offset > tolerance
    ) && (
      secondCurveLocation.offset < (offsetPartLength - tolerance)
    )
  );

  // if (result === true) {
  //   console.warn("*** result is true!!");
  //   console.log(offsetPartLength);
  //   console.log(tolerance)
  // }

  return result;
}

const offsetPartPathIntersectionCheckFactor = 4; // this should ideally be dependent on the angle at which the pathParts meet at the point, but alas, this feature will instead be al dente pasta like in italian restaurant, i.e. spaghetti code, at selected parts.

/**
 * @param {paper.Path[]} partPaths
 * @param {number} shortenPartsSlightlyBy
 * @returns {paper.Path}
 */
function joinFragmentaryPathPartsToClosedPath(partPaths, shortenPartsSlightlyBy=undefined) {
  const finishedPath = new paper.Path();
  finishedPath.visible = false;

  partPaths.forEach(partPath => {
    if (shortenPartsSlightlyBy !== undefined) {
      // removePathTrailingSegments(partPath, 1);
      removePathTrailingLength(partPath, shortenPartsSlightlyBy);
    }
    resetPathDanglingHandles(partPath);
    const newSegments = partPath.segments;
    finishedPath.addSegments(newSegments);
  });
  // finishedPath.addSegments([partPaths[0].segments[0]]);
  finishedPath.closePath();

  // TODO ALEX DELETE
  // partPaths.forEach(part => {
  //   part.visible = true;
  //   part.strokeWidth = 3
  //   part.strokeColor = randomArrayElement(['green', "red", "blue", "yellow", "orange", "cyan"]);
  //   part.opacity = 0.7;
  //   part.bringToFront();
  //   part.selected = true;
  //   // part.fullySelected = true;
  // })

  return finishedPath;
}

/**
 * Probably unnecessary, but automates getting a point that is properly distanced from the path to avoid rounding errors.
 * @param {paper.Path} areaPath
 * @param {paper.Path} path
 */
function calculateWindingNumberForAreaAndPath(areaPath, path) {
  return calculateWindingNumberForPointAndPath(
    getInnerPointInPathArea(areaPath, false),
    path
  );
}

/**
 * Calculates the winding number at the point with the given path, i.e. if the point is p and path is K, then calculates $\omega_{p}(K)$, the number of times that K winds around p in counter-clockwise direction (signed). To do this, we do the classic Point-in-Polygon approach, where we count the crossings of the path with some line connecting the point and some outer point.
 * @param {paper.Point} point
 * @param {paper.Path} path
 */
function calculateWindingNumberForPointAndPath(point, path) {
  if (checkPointIsOnPath(point, path)) {
    throw new Error('calculateWindingNumberForPointAndPath: point is too close to the path');
  }

  const { linePath, lineCrossings } = findLineFromPointToOutsideOfPathWithoutTangentialIntersections(point, path);

  const resultWindingNumber = lineCrossings
    .map(crossing => {
      const crossingTangent = crossing.tangent;
      const crossedTangent = crossing.intersection.tangent;
      return getCrossingNumberOfTangents(crossingTangent, crossedTangent);
    })
    .reduce((a, b) => a + b, 0);

  return resultWindingNumber;
}

/**
 * Returns -1 or +1 depending on whether tCrossed is to the left or to the right, as seen from the direction of tCrossing.
 * @param {paper.Point} tCrossing
 * @param {paper.Point} tCrossed
 */
function getCrossingNumberOfTangents(tCrossing, tCrossed) {
  const directedAngle = tCrossing.getDirectedAngle(tCrossed);
  if (directedAngle >= 0) {
    return -1;
  } else {
    return +1;
  }
}



///*** Obsolete, unused Stuff ***///

/**
 * Just an experiment toggling the paper.Path properties selected and fullySelected on the path and its segments in a staggered, sequential way as a silly animation effect.
 */
function togglePathHandleVisualization() {
  const currentPath = getCurrentImmersion();
  if (currentPath === undefined) {
    return;
  }
  if (currentPath.selected) {
    if (currentPath.fullySelected) {
      togglePathHandleVisualizationHelper(currentPath, false, false);
    } else {
      togglePathHandleVisualizationHelper(currentPath, false, true);
    }
  } else {
    currentPath.fullySelected = false;
    currentPath.selected = true;
  }
}
function togglePathHandleVisualizationHelper(path, selected, fullySelected) {
  if (mySettings.ANIMATIONS_ON) {
    const duration = mySettings.ANIMATIONS_DURATION_MS / path.segments.length;
    path.segments.map((segment, index) => {
      setTimeout(() => {
        segment.fullySelected = fullySelected;
        segment.selected = fullySelected || selected;
      }, duration*index);
    });
    setTimeout(() => {
      path.fullySelected = fullySelected;
      path.selected = fullySelected || selected;
    }, mySettings.ANIMATIONS_DURATION_MS);
  } else {
    path.selected = selected;
    path.fullySelected = fullySelected;
  }
}



export {
  getMouseEventDelta,
  visualizePointWithCircle,
  visualizePointWithMarkerBullsEye,
  pointsEqualsEpsilon,

  addMyPoints,
  paperItemVisibility,
  addGapToPathAtPoint,
  getPathContainsPoint,
  getInnerPointInPathArea,
  getMiddlePointOfPath,
  joinPaths,
  closePathWithoutGap,
  getRotationNumberOfCircleEmbedding,
  checkOnlyTransversalIntersections,
  checkPathIntersectsSelf,
  createPartsClonesAtOffset,
  joinFragmentaryPathPartsToClosedPath,
  calculateWindingNumberForAreaAndPath,
};
