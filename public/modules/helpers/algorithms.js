import MyImmersion from "../classes/myImmersion.js";
import MyImmersionPart from "../classes/myImmersionPart.js";
import MySelfIntersection from "../classes/mySelfIntersection.js";
import MyConnectedComponent from "../classes/myConnectedComponent.js";
import MyOrientedCCImmersionPart from "../classes/myOrientedCCImmersionPart.js";
import MyPathCurveLocation from "../classes/myPathCurveLocation.js";
import MyPathSegment from "../classes/myPathSegment.js";

import { COLOR_PALETTE_DEBUGGING } from "../constants/colors.js";
import { DEBUGGING_VISUALIZE_PATH_PARTS, DEBUGGING_VISUALIZE_PATH_PARTS_SHIFT } from "../constants/debug.js";

import { Sides, isSide } from "../enums/sides.js";
import { StartOrEnd } from "../enums/startOrEnd.js";

import {
  wrappingSlice,
  properMod,
  randomArrayElement,
  arrayUniqueElements,
  last,
} from "../helpers/basic.js";
import {
  pointsEqualsEpsilon,
  getPathContainsPoint,
  getRotationNumberOfCircleEmbedding,
  checkOnlyTransversalIntersections,
} from "../helpers/paper.js"

///*** Special Algorithms, Mathematical Stuff ***///

/**
 * Creates new paper.Path objects that you would get if you split the passed path at all the points of self-intersection.
 *
 * This function assumes that there are only double points, so no point where the path passes three or more times.
 *
 * If there are no selfIntersections, a copy of the original path will be created.
 *
 * The number of new paths should be the same as twice the number of self-intersections, if there are any.
 *
 * @param {MyImmersion} originalImmersion carrying the path to split into parts. The original path will be preserved, since it is cloned before splitting. This object is also used as a reference for the resulting new path parts, so that they have a reference to their parent object.
 * @returns {MyImmersionPart[]}
 */
function createPartsFromPathIntersections(originalImmersion, otherImmersion) {
  const pathClone = originalImmersion.pathClone;
  pathClone.visible = false;
  const createdParts = [];
  const pathSeparationCurveLocations = [];

  if (otherImmersion === undefined) {
    pathSeparationCurveLocations.push(...getCurveLocationsFromSelfIntersections(
      originalImmersion));
  } else {
    // TODO ALEX this has not been tested yet, but written for later use with separating immersion at intersections with line between E and M (i.e. -1 and +1), for the two-center invariants
    pathSeparationCurveLocations.push(...getCurveLocationsFromIntersectionsWithImmersion(originalImmersion, otherImmersion));
  }

  if (pathSeparationCurveLocations.length === 0) {
    // so we just have an embedding of S^1 without intersections
    createdParts.push({ pathPart: pathClone, start: null, end: null })
  } else {
    const addedSegments = addSegmentsToPathAtCurveLocations(
      pathClone, pathSeparationCurveLocations); // new segments are in the order in which they were drawn, the first one starting from the first self intersection one encounters in the finished immersion, when starting at the same position as how it was drawn/created. The start can be slightly off after the simplification of the curve when it is finished.

    createdParts.push(...createPathPartsFromSegments(pathClone, addedSegments));
  }
  if (DEBUGGING_VISUALIZE_PATH_PARTS) {
    const randomColor = randomArrayElement(COLOR_PALETTE_DEBUGGING);
    createdParts.map((resultPart, index) => {
      const visualizePart = resultPart.pathPart.clone();
      visualizePart.strokeColor = randomColor;
      visualizePart.position.x += (index+1) * DEBUGGING_VISUALIZE_PATH_PARTS_SHIFT;
      originalImmersion.addVisibleElement(visualizePart);
    })
  }

  const myImmersionParts = createdParts.map((createdPart, index) => new MyImmersionPart(
    originalImmersion,
    createdPart.pathPart,
    createdPart.start,
    createdPart.end,
    `'${index}`
  ));

  return myImmersionParts;
}

/**
 * @param {MyImmersion} originalImmersion
 * @returns {MyPathCurveLocation[]}
 */
function getCurveLocationsFromSelfIntersections(originalImmersion) {
  return originalImmersion.selfIntersections
    .flatMap(selfInt => [
      new MyPathCurveLocation(selfInt.curveLocations[0], selfInt),
      new MyPathCurveLocation(selfInt.curveLocations[1], selfInt),
    ]);
}

/**
 * @param {MyImmersion} firstImmersion
 * @param {MyImmersion} otherImmersion
 * @returns {MyPathCurveLocation[]}
 */
function getCurveLocationsFromIntersectionsWithImmersion(firstImmersion, otherImmersion) {
  const crossings = checkOnlyTransversalIntersections(firstImmersion, otherImmersion);

  return crossings.map(crossing => new MyPathCurveLocation(crossing, null));
}

/**
 * @param {paper.Path} path
 * @param {MyPathCurveLocation} curveLocations
 * @returns {MyPathSegment[]}
 */
function addSegmentsToPathAtCurveLocations(path, curveLocations) {
  return curveLocations.map(curveLoc => {
    const newSegment = path.divideAt(curveLoc.offset); // this does not split the path, but only inserts a new segment. Later we then create the parts of the path by copying the path between the precise segments that make up the desired parts.

    // TODO ALEX 123 bug: this.segment is null (see screenshot 2024-10-21)
    if (newSegment === null) {
      console.log({newSegment});
      console.log({path});
      console.log({curveLoc});
      console.log({curveLocations});
    }

    return new MyPathSegment(
      newSegment,
      curveLoc.locationOriginObject
    )
  });
}

/**
 * @param {paper.Path} originalPath
 * @param {MyPathSegment[]} pathSegments
 * @returns {MyImmersionPart[]}
 */
function createPathPartsFromSegments(originalPath, pathSegments) {
  const sortedSegments = pathSegments
    .toSorted((s1, s2) => MyPathSegment.sort(s1, s2));

  if (originalPath.closed === true) {
    sortedSegments.push(sortedSegments[0])
  } else {
    const segs = originalPath.segments;
    sortedSegments.unshift(new MyPathSegment(segs[0], null));
    sortedSegments.push(new MyPathSegment(last(segs), null));
  }

  const resultParts = [];

  for (let i = 0; i < sortedSegments.length - 1; i++) {
    const fromSegment = sortedSegments[i];
    const toSegment = sortedSegments[i+1];

    const newPart = new paper.Path({segments: wrappingSlice(
      originalPath.segments,
      fromSegment.segment.index,
      toSegment.segment.index + 1 // +1 to be inclusive
    )});

    resultParts.push({
      pathPart: newPart,
      start: fromSegment.segmentOriginObject,
      end: toSegment.segmentOriginObject
    });
  }

  return resultParts
}


/**
 * Creates MyConnectedComponent objects for the supplied immersion
 *
 * @param {MyImmersion} originalImmersion carrying the path parts to build the connected components from.
 * @param {integer} loopLimit
 * @param {number} epsilonTangentComparison
 * @returns {MyConnectedComponent[]}
 */
function createConnectedComponentsFromPathParts(originalImmersion, loopLimit, epsilonTangentComparison) {
  if (originalImmersion.immersionParts.length < 1) {
    throw new Error('createConnectedComponentsFromPathParts: less than one immersionPart!!!');
  }

  if (originalImmersion.immersionParts.length === 1) {
    // immersion has no self-intersections
    return createConnectedComponentsFromSoloPathPart(originalImmersion);
  }

  return createConnectedComponentsFromMultiplePathParts(originalImmersion, loopLimit, epsilonTangentComparison);
}

/**
 * @param {MyImmersion} originalImmersion carrying the solo path part to build the two connected components from.
 * @returns {MyConnectedComponent[]}
 */
function createConnectedComponentsFromSoloPathPart(originalImmersion) {
  const rotationNumber = getRotationNumberOfCircleEmbedding(originalImmersion.pathClone);
  if (Math.abs(rotationNumber) !== 1) {
    throw new Error('createConnectedComponentsFromSoloPathPart: rotationNumber not -1 or +1.')
  }

  const unboundedComponentIsToTheLeftOfPart = (rotationNumber === -1);
  const unboundedComponentIsToTheRightOfPart = !unboundedComponentIsToTheLeftOfPart;

  const soloPart = originalImmersion.immersionParts[0];
  return [
    new MyConnectedComponent(
      originalImmersion,
      [new MyOrientedCCImmersionPart(soloPart, false, Sides.LEFT)],
      (unboundedComponentIsToTheLeftOfPart ? '#unbounded' : '#0'),
      unboundedComponentIsToTheLeftOfPart
    ),
    new MyConnectedComponent(
      originalImmersion,
      [new MyOrientedCCImmersionPart(soloPart, false, Sides.RIGHT)],
      (unboundedComponentIsToTheRightOfPart ? '#unbounded' : '#0'),
      unboundedComponentIsToTheRightOfPart
    ),
  ]
}

/**
 * @param {MyImmersion} originalImmersion carrying the path parts to build the connected components from.
 * @param {integer} loopLimit
 * @param {number} epsilonTangentComparison
 * @returns {MyConnectedComponent[]}
 */
function createConnectedComponentsFromMultiplePathParts(originalImmersion, loopLimit, epsilonTangentComparison) {
  const connectedComponents = [];
  let loopCounter = 0;

  while (true) {
    if (loopCounter >= loopLimit) {
      throw new Error(`BREAK LOOP createConnectedComponentsFromMultiplePathParts: exceeded loop limit of ${loopLimit}`);
    }

    const { part, side } = findImmersionPartWithMissingConnectedComponent(originalImmersion);

    if (part === undefined) {
      break;
    }
    loopCounter++;

    const newConnectedComponent = findAndCreateConnectedComponentStartingFromPartSide(part, side, loopLimit, epsilonTangentComparison);
    connectedComponents.push(newConnectedComponent);
  }

  const unboundedComponent = findUnboundedComponent(connectedComponents);
  unboundedComponent.isUnboundedComponent = true;
  unboundedComponent.debuggingName = '#unbounded';
  connectedComponents
    .map((cc, index) => {
      if (!cc.isUnboundedComponent) {
        cc.debuggingName = `#${index}`
      }
    });

  return connectedComponents;
}

/**
 * @param {MyImmersion} originalImmersion
 * @returns {{part: MyImmersionPart, side: Sides}}
 */
function findImmersionPartWithMissingConnectedComponent(originalImmersion) {
  for (const immersionPart of originalImmersion.immersionParts) {
    if (immersionPart.leftConnectedComponent === undefined) {
      return { part: immersionPart, side: Sides.LEFT }
    }
    if (immersionPart.rightConnectedComponent === undefined) {
      return { part: immersionPart, side: Sides.RIGHT }
    }
  }

  return { part: undefined, side: undefined };
}


/**
 * @param {MyImmersionPart} immersionPart
 * @param {Sides} side
 * @param {integer} loopLimit
 * @param {number} epsilonTangentComparison
 * @returns {MyConnectedComponent}
 */
function findAndCreateConnectedComponentStartingFromPartSide(immersionPart, side, loopLimit, epsilonTangentComparison) {
  checkSideOk(immersionPart, side);

  const firstPart = new MyOrientedCCImmersionPart(immersionPart, false, side);

  let currentPart = firstPart;
  const orientedImmersionParts = [];
  let loopCounter = 0;

  while (true) {
    if (loopCounter >= loopLimit) {
      // TODO ALEX 123 Error0.5 <- draw K_2, create 3-Bifurcation, will not finish adding path parts, maybe visualize:
      orientedImmersionParts.forEach((part, index) => {
        if (index <= 5) {
          part.adjustedPartPath.visible = true;
          part.adjustedPartPath.strokeWidth = 'black';
          part.adjustedPartPath.strokeWidth = 5;
        }
      })
      console.log({orientedImmersionParts});
      throw new Error(`BREAK LOOP findAndCreateConnectedComponentStartingFromPartSide: exceeded loop limit of ${loopLimit}`);
    }
    loopCounter++;

    orientedImmersionParts.push(currentPart);

    const nextIntersection = currentPart.adjustedEndPoint;

    const candidatePartsAndTheirOutgoingTangents = getIntersectionImmersionPartsWithTangents(nextIntersection, side);

    const currentOutwardsTangentAtNextIntersection = currentPart.adjustedEndTangent.multiply(-1);

    const selectedCandidatePart = getNextTangentPart(
      currentOutwardsTangentAtNextIntersection,
      candidatePartsAndTheirOutgoingTangents,
      side,
      epsilonTangentComparison
    );

    currentPart = selectedCandidatePart;

    if (currentPart.adjustedPartPath.compare(firstPart.adjustedPartPath)) { // see http://paperjs.org/reference/pathitem/#compare-path
      // the loop returned to the first immersionPart, so let's break
      break;
    }
  }

  return new MyConnectedComponent(
    immersionPart.parentImmersion,
    orientedImmersionParts
  );
}

/**
 * @param {MyImmersionPart} immersionPart
 * @param {Sides} side
 */
function checkSideOk(immersionPart, side) {
  if (
    (side === Sides.LEFT && immersionPart.leftConnectedComponent !== undefined)
    && (side === Sides.RIGHT && immersionPart.rightConnectedComponent !== undefined)
  ) {
    throw new Error(`checkSideOk: connected component for side ${side} already exists, abort!`);
  }
  if (!isSide(side)) {
    throw new Error('checkSideOk: side neither LEFT nor RIGHT wth.')
  }
}

/**
 * Starting from a selfIntersection, return all the outgoing immersionParts (possibly double occurences) and their tangent vectors at the selfIntersection, each oriented away from the point.
 * @param {MySelfIntersection} selfIntersection
 * @param {Sides} side
 * @returns {{orientedPart: MyOrientedCCImmersionPart, tangent: path.Point}[]}
 */
function getIntersectionImmersionPartsWithTangents(selfIntersection, side) {
  return arrayUniqueElements(selfIntersection.immersionParts)
    .flatMap(immPart => {
      const returnedPartsWithTangent = []

      if (immPart.startPoint === selfIntersection) {
        returnedPartsWithTangent.push(
          createOrientedImmersionPartWithTangent(immPart, StartOrEnd.START, side)
        );
      }
      if (immPart.endPoint === selfIntersection) {
        returnedPartsWithTangent.push(
          createOrientedImmersionPartWithTangent(immPart, StartOrEnd.END, side)
        );
      }
      return returnedPartsWithTangent;
    });
}

/**
 * @param {MyImmersionPart} part
 * @param {StartOrEnd} startOrEnd
 * @param {Sides} side
 * @returns {MyOrientedCCImmersionPart}
 */
function createOrientedImmersionPartWithTangent(part, startOrEnd, side) {
  return {
    orientedPart: new MyOrientedCCImmersionPart(
      part,
      (startOrEnd === StartOrEnd.END ? true : false),
      side
    ),
    tangent: (
      startOrEnd === StartOrEnd.END ?
      part.endTangent.multiply(-1) :
      part.startTangent
    )
  }
}

/**
 * @param {paper.Point} referenceTangent
 * @param {{orientedPart: MyOrientedCCImmersionPart, tangent: path.Point}[]} candidatePartsAndTheirOutgoingTangents
 * @param {Sides} side
 * @param {number} epsilonTangentComparison
 * @returns {MyOrientedCCImmersionPart}
 */
function getNextTangentPart(
  referenceTangent,
  candidatePartsAndTheirOutgoingTangents,
  side,
  epsilonTangentComparison
) {
  const filteredCandidates = checkIntersectionTangentsOk(
    referenceTangent,
    candidatePartsAndTheirOutgoingTangents,
    epsilonTangentComparison
  );

  const sortedCandidates = filteredCandidates
    .map(fc => {
      const signedAngle = referenceTangent.getDirectedAngle(fc.tangent);
      const sideMultiplier = (side === Sides.RIGHT ? +1 : -1);
      const unsignedAngle = properMod(signedAngle * sideMultiplier, 360);

      return { ...fc, distanceDegrees: unsignedAngle };
    })
    .toSorted((fc1, fc2) => fc1.distanceDegrees - fc2.distanceDegrees);


  // clean up
  for (let i = 1; i < sortedCandidates.length; i++) {
    sortedCandidates[i].orientedPart.destroySelf();
  }

  return sortedCandidates[0].orientedPart;
}

/**
 *
 * @param {paper.Point} referenceTangent
 * @param {{part: MyOrientedCCImmersionPart, tangent: path.Point}[]} candidatePartsAndTheirOutgoingTangents
 * @param {number} epsilonTangentComparison
 * @returns {{part: MyOrientedCCImmersionPart, tangent: path.Point}[]} filtered candidates
 */
function checkIntersectionTangentsOk(referenceTangent, candidatePartsAndTheirOutgoingTangents, epsilonTangentComparison) {
  if (candidatePartsAndTheirOutgoingTangents < 4) {
    throw new Error('checkIntersectionTangentsOk: not enough tangents, must be at least 4.')
  }
  const filteredCandidates = candidatePartsAndTheirOutgoingTangents
    .filter(cpot => !pointsEqualsEpsilon(cpot.tangent, referenceTangent, epsilonTangentComparison));
  if (filteredCandidates === candidatePartsAndTheirOutgoingTangents.length) {
    throw new Error('checkIntersectionTangentsOk: referenceTangent not included in all tangents.')
  }
  if (filteredCandidates < candidatePartsAndTheirOutgoingTangents.length - 1) {
    throw new Error('checkIntersectionTangentsOk: referenceTangent included multiple times in all tangents (at least within given epsilon bounds), which indicates that the crossing is in fact a tangential crossing, which we do not allow. TODO: perturb this instead of throwing an error. Until then just avoid this case.')
  }

  return filteredCandidates;
}

/**
 * Find the unbounded component.
 * To do this, check if there is a component that contains some part path that is not in the component's boundary. We do this by checking if that part's middlePoint is inside. This can only be true for the area described by the boundary of the unbounded component, since all other component boundaries do not contain any other path parts.
 * But if all part paths only have one bounded component on one side and on the other side the unbounded component (i.e. there is no part between two bounded components), then the unbounded component uses all parts as boundary. For that case, check for the component with the most parts.
 * Use this function only once all connected components are created.
 * @param {MyConnectedComponent[]} connectedComponents
 * @returns {MyConnectedComponent}
 */
function findUnboundedComponent(connectedComponents) {
  const unboundedComponent = checkUnboundedComponent(connectedComponents);
  if (unboundedComponent !== undefined) {
    return unboundedComponent;
  }

  const sortedComponentsByPartAmounts = connectedComponents
    .map(cc => ({ component: cc, partsAmount: cc.orientedImmersionParts.length }))
    .toSorted((ccp1, ccp2) => ccp2.partsAmount - ccp1.partsAmount) // descending sort
    .map(ccp => ccp.component);

  // first check
  for (let componentIndex = 0; componentIndex < sortedComponentsByPartAmounts.length; componentIndex++) {
    const currentComponent = sortedComponentsByPartAmounts[componentIndex];
    const otherParts = currentComponent.getAllOtherOriginalImmersionParts();

    let componentContainsOtherPart = false;
    for (let otherPartIndex = 0; otherPartIndex < otherParts.length; otherPartIndex++) {
      const otherPart = otherParts[otherPartIndex];
      const middlePoint = otherPart.middlePoint;

      if (getPathContainsPoint(currentComponent.boundaryPath, middlePoint)) {
        componentContainsOtherPart = true;
        break;
      }
    }

    if (componentContainsOtherPart) {
      return currentComponent;
    }
  }

  // second check
  return sortedComponentsByPartAmounts[0];
}

/**
 * @param {MyConnectedComponent[]} connectedComponents
 */
function checkUnboundedComponent(connectedComponents) {
  const unboundedComponents = connectedComponents
    .filter(cc => cc.isUnboundedComponent);
  if (unboundedComponents.length === 1) {
    return unboundedComponents[0];
  }
  if (unboundedComponents.length > 1) {
    throw new Error('findUnboundedComponent: too many unbounded components.');
  }
  return undefined;
}


export {
  createPartsFromPathIntersections,
  getCurveLocationsFromSelfIntersections,
  getCurveLocationsFromIntersectionsWithImmersion,
  addSegmentsToPathAtCurveLocations,
  createPathPartsFromSegments,
  createConnectedComponentsFromPathParts,
  createConnectedComponentsFromSoloPathPart,
  createConnectedComponentsFromMultiplePathParts,
  findImmersionPartWithMissingConnectedComponent,
  findAndCreateConnectedComponentStartingFromPartSide,
  checkSideOk,
  getIntersectionImmersionPartsWithTangents,
  createOrientedImmersionPartWithTangent,
  getNextTangentPart,
  checkIntersectionTangentsOk,
  findUnboundedComponent,
  checkUnboundedComponent,
};
