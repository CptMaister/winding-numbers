
///*** Example Curves and Values ***///

// ASCII Sketch of this curve:
//  ____________
// |            |
// |     /\     |
// |    /  \    |
// |    \  /    |
// |     \/     |
// |     /\     |
// |____/  \  __|
//          \/   
//          /\   
//          \/   
//               
const examplePolygonalCurveDoubleDip = {
  name: 'examplePolygonalCurveDoubleDip',
  points: [
    {x: 0, y: 0},
    {x: 0, y: 100},

    {x: 30, y: 100},
    {x: 80, y: 50},

    {x: 50, y: 20},

    {x: 20, y: 50},
    {x: 70, y: 100},

    {x: 90, y: 120},
    {x: 80, y: 140},
    {x: 70, y: 120},
    {x: 90, y: 100},

    {x: 100, y: 100},
    {x: 100, y: 0},
    // {x: 0, y: 0}, // the curve is expected to be closed automatically
  ],
  baseProperties: {
    noTangentialCrossings: true, // TODO write test with tangential crossings
    crossingPoints: [
      {x: 50, y: 80},
      {x: 80, y: 110},
    ],
    immersionPartsCount: 4,
    connectedComponentsCount: 4,
    connectedComponentWindingNumbers: [0, 1, 2, -1],
    doublePointIndices: [1, 0],
    jPlus: -2,
  }
};

// ASCII Sketch of this curve:
//  _____       
// |     |      
// |     |      
// |_____|_____ 
//       |     |
//       |     |
//       |_____|
//              
const examplePolygonalCurveK0 = { // TODO use this curve in tests
  name: 'examplePolygonalCurveK0',
  points: [
    {x: 0, y: 0},
    {x: 0, y: 50},
    {x: 100, y: 50},

    {x: 100, y: 100},

    {x: 50, y: 100},
    {x: 50, y: 0},
    // {x: 0, y: 0}, // the curve is expected to be closed automatically
  ],
  baseProperties: {
    noTangentialCrossings: true,
    crossingPoints: [ {x: 50, y: 50} ],
    immersionPartsCount: 2,
    connectedComponentsCount: 3,
    connectedComponentWindingNumbers: [0, 1, -1],
    doublePointIndices: [0],
    jPlus: 0,
  }
};

// ASCII Sketch of this curve:
//  ____________
// |            |
// |            |
// |            |
// |            |
// |            |
// |____________|
//               
const examplePolygonalCurveK1 = { // TODO use this curve in tests
  name: 'examplePolygonalCurveK1',
  points: [
    {x: 0, y: 0},
    {x: 0, y: 100},
    {x: 100, y: 100},
    {x: 100, y: 0},
    // {x: 0, y: 0}, // the curve is expected to be closed automatically
  ],
  baseProperties: {
    noTangentialCrossings: true,
    crossingPoints: [],
    immersionPartsCount: 1,
    connectedComponentsCount: 2,
    connectedComponentWindingNumbers: [0, 1],
    doublePointIndices: [],
    jPlus: 0,
  }
};

// ASCII Sketch of this curve:
//  ____________
// |            |
// |     /\     |
// |    /  \    |
// |    \  /    |
// |     \/     |
// |     /\     |
// |____/  \____|
//               
const examplePolygonalCurveK2 = { // TODO use this curve in tests
  name: 'examplePolygonalCurveK2',
  points: [
    {x: 0, y: 0},
    {x: 0, y: 100},

    {x: 30, y: 100},
    {x: 80, y: 50},

    {x: 50, y: 20},

    {x: 20, y: 50},
    {x: 70, y: 100},

    {x: 100, y: 100},
    {x: 100, y: 0},
    // {x: 0, y: 0}, // the curve is expected to be closed automatically
  ],
  baseProperties: {
    noTangentialCrossings: true,
    crossingPoints: [ {x: 50, y: 80} ],
    immersionPartsCount: 2,
    connectedComponentsCount: 3,
    connectedComponentWindingNumbers: [0, 1, 2],
    doublePointIndices: [1],
    jPlus: -2,
  }
};

// ASCII Sketch of this curve:
//  ____________
// |            |
// |     /\     |
// |    /  \    |
// |    \  /    |
// |     \/     |
// |     /\     |
// |    /  \    |
// |    \  /    |
// |     \/     |
// |_____/\_____|
//               
const examplePolygonalCurveNeckPillow = { // TODO use this curve in tests
  name: 'examplePolygonalCurveNeckPillow',
  points: [
    {x: 0, y: 0},
    {x: 0, y: 100},

    {x: 40, y: 100},
    {x: 60, y: 80},
    {x: 20, y: 40},

    {x: 50, y: 10},

    {x: 80, y: 40},
    {x: 40, y: 80},
    {x: 60, y: 100},

    {x: 100, y: 100},
    {x: 100, y: 0},
    // {x: 0, y: 0}, // the curve is expected to be closed automatically
  ],
  baseProperties: {
    noTangentialCrossings: true,
    crossingPoints: [
      {x: 50, y: 90},
      {x: 50, y: 70},
    ],
    immersionPartsCount: 4,
    connectedComponentsCount: 4,
    connectedComponentWindingNumbers: [0, 1, 2, 0],
    doublePointIndices: [1, 1],
    jPlus: 0,
  }
};

// ASCII Sketch of this curve:
//  ____________
// |            |
// |     /\     |
// |    /  \    |
// |    \  /    |
// |     \/     |
// |     /\     |
// |____|__|____| // <- here we have continuously many tangential intersections
//               
const examplePolygonalCurveDegenerate = { // TODO use this curve in tests
  name: 'examplePolygonalCurveDegenerate',
  points: [
    {x: 0, y: 0},
    {x: 0, y: 100},

    {x: 10, y: 100},
    {x: 70, y: 100},
    {x: 80, y: 80},
    {x: 20, y: 40},

    {x: 50, y: 20},

    {x: 80, y: 40},
    {x: 20, y: 80},
    {x: 30, y: 100},
    {x: 90, y: 100},

    {x: 100, y: 100},
    {x: 100, y: 0},
    // {x: 0, y: 0}, // the curve is expected to be closed automatically
  ],
  baseProperties: {
    noTangentialCrossings: false,
  }
};

// TODO: create smooth non-generic curve, not just a polygonal approximation!!

const allExampleCurves = [
  examplePolygonalCurveDoubleDip,
  examplePolygonalCurveK0,
  examplePolygonalCurveK1,
  examplePolygonalCurveK2,
  examplePolygonalCurveNeckPillow,
  examplePolygonalCurveDegenerate
]



const exampleAnalyzePolygonalCurveA = {
  polygonalCurve: examplePolygonalCurveDoubleDip.points,
  originPoint: { x: 50, y: 50 },
  expectedResults: {
    ...examplePolygonalCurveDoubleDip.baseProperties,
    j1: 0,
    j2: 0, // check for yourself at https://complex-fibers.org/
  }
};

const exampleAnalyzePolygonalCurveB = { // TODO test this, different values for j1 and j2
  polygonalCurve: examplePolygonalCurveDoubleDip.points,
  originPoint: { x: 50, y: 100 },
  expectedResults: {
    ...examplePolygonalCurveDoubleDip.baseProperties,
    j1: -2,
    j2: -2,
  }
};

const exampleAnalyzePolygonalCurveC = { // TODO test this, different values for j1 and j2
  polygonalCurve: examplePolygonalCurveDoubleDip.points,
  originPoint: { x: 10, y: 10 },
  expectedResults: {
    ...examplePolygonalCurveDoubleDip.baseProperties,
    j1: -1.5,
    j2: -4,
  }
};

export {
  examplePolygonalCurveDoubleDip,
  examplePolygonalCurveK0,
  examplePolygonalCurveK1,
  examplePolygonalCurveK2,
  examplePolygonalCurveNeckPillow,
  examplePolygonalCurveDegenerate,
  allExampleCurves,

  exampleAnalyzePolygonalCurveA,
  exampleAnalyzePolygonalCurveB,
  exampleAnalyzePolygonalCurveC,
};