import { analyzeExampleCurve } from "../api.js";
import { allExampleCurves } from "./cases.js";
import { DEBUGGING_TEST_CURVES_DESTROY_EVERYTHING } from "../constants/debug.js";


//////////////////////////////
///------------------------///
///                        ///
///  ~~~  Test Suite  ~~~  ///
///                        ///
///------------------------///
//////////////////////////////


///*** Basic Test Functions ***///

function runTests(runAllTests=true) {
  const allTests = [
    dummyFakeTest,
    // testAnalyzePolygonalCurves_complete_success,
    () => testAnalyzePolygonalCurves_byProperty('noTangentialCrossings'),
    () => testAnalyzePolygonalCurves_byProperty('crossingPoints'),
    () => testAnalyzePolygonalCurves_byProperty('immersionPartsCount'),
    () => testAnalyzePolygonalCurves_byProperty('connectedComponentsCount'),
    () => testAnalyzePolygonalCurves_byProperty('connectedComponentWindingNumbers'),
    () => testAnalyzePolygonalCurves_byProperty('doublePointIndices'),
    () => testAnalyzePolygonalCurves_byProperty('jPlus', true),
  ];

  const runTheseTests = runAllTests ? allTests : [dummyFakeTest];

  const testResults = [];
  runTheseTests.map(test => {
    const testResult = test();
    (Array.isArray(testResult) ? testResult : [testResult]).map(result => {
      testResults.push(result);
    });
  })

  const countFailed = testResults.filter(r => !r.passed).length;
  const countSuccess = testResults.filter(r => r.passed).length;

  // console.log('Hello!\nIf you want to modify the code of this site, feel free to do whatever with it and reach out to me if you need any help: alex.mai@posteo.net\nI implemented a tiny custom test suite that checks if the proper results come out at the end, but all but a single dummy test are disabled by default, so that the end user devices don\'t do all these calculations on every page load. You can reenable them by setting the 'runAllTests' flag to true in the 'runTests()' function.\nHave fun!') // TODO END readd message
  console.info(`Finished running ${runTheseTests.length} (bundled) tests. Ignored ${allTests.length - runTheseTests.length} test.`)
  console.info(`%cFailed: ${countFailed}. Success: ${countSuccess}.`,
    `background: ${countFailed === 0 ? 'green' : 'red'}; color: white; font-size: 150%; font-weight: bold;`
  )
  if (countFailed > 0) {
    console.table(testResults
      .map(res => ({ passed: res.passed, ...res }))
      .toSorted(sortTestResults));
  }
}


///*** Test Cases ***///

function dummyFakeTest() {
  return {
    name: 'dummyFakeTest',
    passed: true,
    notes: 'This is just a fake test for test testing reasons.'
  }
}

function testAnalyzePolygonalCurves_complete_success() {
  const examplePolygonalCurve = exampleAnalyzePolygonalCurveA.polygonalCurve
  const analysisResults = analyzeExampleCurve(examplePolygonalCurve, undefined, undefined, DEBUGGING_TEST_CURVES_DESTROY_EVERYTHING);
  const expectedResults = exampleAnalyzePolygonalCurveA.expectedResults;

  return compareActualAndExpectedResults(
    'testAnalyzePolygonalCurveA_complete_success',
    analysisResults,
    expectedResults
  );
}

function testAnalyzePolygonalCurves_byProperty(propertyName, visible=false) {
  return allExampleCurves.map((exampleCurve, index) => {
    const shiftCurvePosition = (visible ? {x: 15 + index* 110, y: 15} : undefined);

    const analysisResults = analyzeExampleCurve(exampleCurve.points, shiftCurvePosition, undefined, DEBUGGING_TEST_CURVES_DESTROY_EVERYTHING);

    const expectedResults = {}
    expectedResults[propertyName] = exampleCurve.baseProperties[propertyName]

    return compareActualAndExpectedResults(
      `testAnalyzePolygonalCurves_${propertyName}_${exampleCurve.name}`,
      analysisResults,
      expectedResults
    );
  })
}




export default runTests;
