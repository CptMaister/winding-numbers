
function sortTestResults(result1, result2) {
  // sort first by passed boolean, then subsort names alphabetically
  return (result1.passed === result2.passed) ?
    result1.name.localeCompare(result2.name) : (
      result1.passed === true ? 1 : 0
    );
}


function compareActualAndExpectedResults(name, actualObject, expectedObject) {
  const failedComparisons = [];
  Object.entries(expectedObject).map(([key, value]) => {
    let successfulComparison = false;

    if (value !== undefined && Array.isArray(value)) {
      successfulComparison = arraysEqualXY(value, actualObject[key], true)
    } else {
      successfulComparison = (actualObject[key] === value);
    }

    if (!successfulComparison) {
      const failedComparison = {
        key,
        expected: value,
        actual: actualObject[key]
      }
      failedComparisons.push(failedComparison);
    }
  })

  const failureNotes = failedComparisons.map(comparison => {
    return `${comparison.key}, expected: ${toStringHelper(comparison.expected)}, actual: ${toStringHelper(comparison.actual)}`
  }).join(';\n')

  return {
    name: name,
    passed: failedComparisons.length === 0,
    notes: failureNotes
  }
}

function toStringHelper(thingOrThings) {
  if (Array.isArray(thingOrThings)) {
    return '[' + thingOrThings.map(thing => toStringXYHelper(thing)).join(', ') + ']';
  } else {
    return toStringXYHelper(thingOrThings);
  }
}
function toStringXYHelper(thing) {
  if (thing !== undefined && thing.x !== undefined) {
    return `{x: ${thing.x}, y: ${thing.y}}`
  } else {
    return thing;
  }
}


export {
  sortTestResults,
  compareActualAndExpectedResults,
};