import MyDrawingView from "../classes/myDrawingView.js";


const VisualsImmersion = Object.freeze({
  PATH: {
    symbol: Symbol('path'),
    handler: handleVisualsImmersionPath,
    default: true,
  },
  PATH_INTERSECTIONS: {
    symbol: Symbol('path-intersections'),
    handler: handleVisualsImmersionPathIntersections,
    default: true,
  },
  PATH_SEGMENTS: {
    symbol: Symbol('path-segments'),
    handler: handleVisualsImmersionPathSegments,
    default: false,
  },
  PATH_SEGMENT_HANDLES: {
    symbol: Symbol('path-segment-handles'),
    handler: handleVisualsImmersionPathSegmentHandles,
    default: false,
  },
  PATH_PARTS: {
    symbol: Symbol('path-parts'),
    handler: handleVisualsImmersionPathParts,
    default: false,
  },
  PATH_ORIENTATION: {
    symbol: Symbol('path-orientation'),
    handler: handleVisualsImmersionPathOrientation,
    default: false,
  },
  CONNECTED_COMPONENT_AREAS: {
    symbol: Symbol('cc-areas'),
    handler: handleVisualsImmersionCCAreas,
    default: true,
  },
  CONNECTED_COMPONENT_BOUNDARIES: {
    symbol: Symbol('cc-boundaries'),
    handler: handleVisualsImmersionCCBoundaries,
    default: false,
  },
  CONNECTED_COMPONENT_WINDING_NUMBERS: {
    symbol: Symbol('cc-winding-numbers'),
    handler: handleVisualsImmersionCCWindingNumbers,
    default: true,
  },
  DOUBLE_POINT_INDICES: {
    symbol: Symbol('dp-indices'),
    handler: handleVisualsImmersionDPIndices,
    default: false,
  },
  BRANCHES_LEVI_CIVITA: {
    symbol: Symbol('lc-branches'),
    handler: handleVisualsImmersionLCBranches,
    default: false,
  },
  BRANCHES_BIRKHOFF: {
    symbol: Symbol('birkhoff-branches'),
    handler: handleVisualsImmersionBirkhoffBranches,
    default: false,
  },
});

/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsImmersionPath(myDrawingView, visible) {
  myDrawingView.handleVisualsImmersionPath(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsImmersionPathIntersections(myDrawingView, visible) {
  myDrawingView.handleVisualsImmersionPathIntersections(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsImmersionPathSegments(myDrawingView, visible) {
  const segmentHandlesBool = myDrawingView.getVisualsImmersionCheckboxChecked(VisualsImmersion.PATH_SEGMENT_HANDLES);
  myDrawingView.handleVisualsImmersionPathSegmentsAndHandles(visible, segmentHandlesBool);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsImmersionPathSegmentHandles(myDrawingView, visible) {
  const segmentsBool = myDrawingView.getVisualsImmersionCheckboxChecked(VisualsImmersion.PATH_SEGMENTS);
  myDrawingView.handleVisualsImmersionPathSegmentsAndHandles(segmentsBool, visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsImmersionPathParts(myDrawingView, visible) {
  myDrawingView.handleVisualsImmersionPathParts(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsImmersionPathOrientation(myDrawingView, visible) {
  myDrawingView.handleVisualsImmersionPathOrientation(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsImmersionCCAreas(myDrawingView, visible) {
  myDrawingView.handleVisualsImmersionCCAreas(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsImmersionCCBoundaries(myDrawingView, visible) {
  myDrawingView.handleVisualsImmersionCCBoundaries(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsImmersionCCWindingNumbers(myDrawingView, visible) {
  myDrawingView.handleVisualsImmersionCCWindingNumbers(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsImmersionDPIndices(myDrawingView, visible) {
  myDrawingView.handleVisualsImmersionDPIndices(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsImmersionLCBranches(myDrawingView, visible) {
  myDrawingView.handleVisualsImmersionLCBranches(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsImmersionBirkhoffBranches(myDrawingView, visible) {
  myDrawingView.handleVisualsImmersionBirkhoffBranches(visible);
}


const VisualsView = Object.freeze({
  ORIGIN: {
    symbol: Symbol('origin'),
    handler: handleVisualsRegularizationOrigin,
    default: false,
  },
  TARGET: {
    symbol: Symbol('target'),
    handler: handleVisualsRegularizationTarget,
    default: false,
  },
  TWO_CENTER_E: {
    symbol: Symbol('two-center-e'),
    handler: handleVisualsRegularizationTwoCenterE,
    default: false,
  },
  TWO_CENTER_M: {
    symbol: Symbol('two-center-m'),
    handler: handleVisualsRegularizationTwoCenterM,
    default: false,
  },
  TWO_CENTER_EM_LINE_SEGMENT: {
    symbol: Symbol('two-center-em-line'),
    handler: handleVisualsRegularizationTwoCenterEMLine,
    default: false,
  },
  X_POSITIVE_RAY: {
    symbol: Symbol('x-pos'),
    handler: handleVisualsRegularizationXPositive,
    default: false,
  },
});


/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsRegularizationOrigin(myDrawingView, visible) {
  myDrawingView.handleVisualsRegularizationOrigin(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsRegularizationTarget(myDrawingView, visible) {
  myDrawingView.handleVisualsRegularizationTarget(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsRegularizationTwoCenterE(myDrawingView, visible) {
  myDrawingView.handleVisualsRegularizationTwoCenterE(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsRegularizationTwoCenterM(myDrawingView, visible) {
  myDrawingView.handleVisualsRegularizationTwoCenterM(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsRegularizationTwoCenterEMLine(myDrawingView, visible) {
  myDrawingView.handleVisualsRegularizationTwoCenterEMLine(visible);
}
/**
 * @param {MyDrawingView} myDrawingView 
 * @param {boolean} visible 
 */
function handleVisualsRegularizationXPositive(myDrawingView, visible) {
  myDrawingView.handleVisualsRegularizationXPositive(visible);
}


export { VisualsImmersion, VisualsView };
