const FontFamily = {
  ARIAL: 'Arial', // sans-serif
  VERDANA: 'Verdana', // sans-serif
  TAHOMA: 'Tahoma', // sans-serif
  TREBUCHET_MS: 'Trebuchet MS', // sans-serif
  TIMES_NEW_ROMAN: 'Times New Roman', // serif
  GEORGIA: 'Georgia', // serif
  GARAMOND: 'Garamond', // serif
  COURIER_NEW: 'Courier New', // monospace
  BRUSH_SCRIPT_MT: 'Brush Script MT', // cursive
};

const FontWeight = {
  NORMAL: 'normal',
  BOLD: 'bold',
};

export { FontFamily, FontWeight };
