
const MouseMode = Object.freeze({
  IMMERSION_NEW: Symbol('immersion-draw-new'),
  IMMERSION_DRAG: Symbol('immersion-drag'),
  IMMERSION_SELECT: Symbol('immersion-select'),
  IMMERSION_RESIZE: Symbol('immersion-resize'),
  IMMERSION_EDIT_SEGMENTS: Symbol('immersion-edit-segments'),
  IMMERSION_EDIT_HANDLES: Symbol('immersion-edit-handles'),

  VIEW_PAN: Symbol('canvas-pan'),
  VIEW_ZOOM: Symbol('canvas-zoom'),
  PLACE_ORIGIN: Symbol('place-origin'),
  PLACE_TARGET: Symbol('place-target'),
  PLACE_TWO_CENTER_E: Symbol('place-two-center-e'),
  PLACE_TWO_CENTER_M: Symbol('place-two-center-m'),
});

const MouseModesAvailability = {
  ALWAYS: [
    MouseMode.IMMERSION_NEW,
    MouseMode.VIEW_PAN,
    MouseMode.VIEW_ZOOM,
    MouseMode.PLACE_ORIGIN,
    MouseMode.PLACE_TARGET,
    MouseMode.PLACE_TWO_CENTER_E,
    MouseMode.PLACE_TWO_CENTER_M,
  ],
  IMMERSIONS_EXIST: [
    MouseMode.IMMERSION_SELECT,
  ],
  IMMERSION_SELECTED: [
    MouseMode.IMMERSION_DRAG,
    MouseMode.IMMERSION_RESIZE,
    MouseMode.IMMERSION_EDIT_SEGMENTS,
    MouseMode.IMMERSION_EDIT_HANDLES,
  ],
}

/**
 * @param {MouseMode} mouseMode
 * @param {boolean} isClicked
 * @returns {string}
 */
function getClassForMouseMode(mouseMode, isClicked=false) {
  switch (mouseMode) {
    case MouseMode.IMMERSION_NEW:
      return 'mouse-new';
    case MouseMode.IMMERSION_DRAG:
      return getClassForMouseModeDrag(isClicked);
    case MouseMode.IMMERSION_SELECT:
      return 'mouse-select';
    case MouseMode.VIEW_PAN:
      return getClassForMouseModeDrag(isClicked);
    // case MouseMode.:
    //   return 'mouse-';
    // case MouseMode.:
    //   return 'mouse-';
    //   // TODO ALEX CONTINUE
    default:
      return 'mouse-default';
  }
}
function getClassForMouseModeDrag(isClicked=false) {
  if (isClicked) {
    return 'mouse-dragging';
  } else {
    return 'mouse-drag';
  }
}

export { MouseMode, MouseModesAvailability, getClassForMouseMode };
