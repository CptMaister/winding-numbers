
const StartOrEnd = Object.freeze({
  START: Symbol('start'),
  END: Symbol('end'),
});
function isStartOrEnd(startOrEnd) {
  return [StartOrEnd.LEFT, StartOrEnd.RIGHT].includes(startOrEnd);
}

export { StartOrEnd, isStartOrEnd };
