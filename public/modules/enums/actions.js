import MyDrawingView from "../classes/myDrawingView.js";

const Actions = Object.freeze({
  CANVAS_RESET: {
    symbol: Symbol('canvas-reset'),
    handler: handleButtonCanvasReset,
  },
  IMMERSION_DELETE: {
    symbol: Symbol('immersion-delete'),
    handler: handleButtonImmersionDelete,
  },
  IMMERSION_DUPLICATE: {
    symbol: Symbol('immersion-duplicate'),
    handler: handleButtonImmersionDuplicate,
  },
  IMMERSION_SMOOTH: {
    symbol: Symbol('immersion-smooth'),
    handler: handleButtonImmersionSmooth,
  },
  IMMERSION_FLATTEN: {
    symbol: Symbol('immersion-flatten'),
    handler: handleButtonImmersionFlatten,
  },
  IMMERSION_SIMPLIFY: {
    symbol: Symbol('immersion-simplify'),
    handler: handleButtonImmersionSimplify,
  },
  IMMERSION_BRING_FORWARD: {
    symbol: Symbol('immersion-bring-forward'),
    handler: handleButtonImmersionBringForward,
  },
  IMMERSION_SEND_BACK: {
    symbol: Symbol('immersion-send-back'),
    handler: handleButtonImmersionSendBack,
  },
  IMMERSION_UNDO: {
    symbol: Symbol('immersion-undo'),
    handler: handleButtonImmersionUndo,
  },
  IMMERSION_REDO: {
    symbol: Symbol('immersion-redo'),
    handler: handleButtonImmersionRedo,
  },
  CANVAS_EXPORT_SVG: {
    symbol: Symbol('canvas-export-svg'),
    handler: handleButtonCanvasExportSvg,
  },
  CANVAS_IMPORT_SVG: {
    symbol: Symbol('canvas-import-svg'),
    handler: handleButtonCanvasImportSvg,
  },
});

/**
 * @param {MyDrawingView} myDrawingView 
 */
function handleButtonCanvasReset(myDrawingView) {
  myDrawingView.handleCanvasReset();
}
/**
 * @param {MyDrawingView} myDrawingView 
 */
function handleButtonImmersionDelete(myDrawingView) {
  myDrawingView.handleImmersionDelete();
}
/**
 * @param {MyDrawingView} myDrawingView 
 */
function handleButtonImmersionDuplicate(myDrawingView) {
  myDrawingView.handleImmersionDuplicate();
}
/**
 * @param {MyDrawingView} myDrawingView 
 */
function handleButtonImmersionSmooth(myDrawingView) {
  myDrawingView.handleImmersionSmooth();
}
/**
 * @param {MyDrawingView} myDrawingView 
 */
function handleButtonImmersionFlatten(myDrawingView) {
  myDrawingView.handleImmersionFlatten();
}
/**
 * @param {MyDrawingView} myDrawingView 
 */
function handleButtonImmersionSimplify(myDrawingView) {
  myDrawingView.handleImmersionSimplify();
}
/**
 * @param {MyDrawingView} myDrawingView 
 */
function handleButtonImmersionBringForward(myDrawingView) {
  myDrawingView.handleImmersionBringForward();
}
/**
 * @param {MyDrawingView} myDrawingView 
 */
function handleButtonImmersionSendBack(myDrawingView) {
  myDrawingView.handleImmersionSendBack();
}
/**
 * @param {MyDrawingView} myDrawingView 
 */
function handleButtonImmersionUndo(myDrawingView) {
  myDrawingView.handleImmersionUndo();
}
/**
 * @param {MyDrawingView} myDrawingView 
 */
function handleButtonImmersionRedo(myDrawingView) {
  myDrawingView.handleImmersionRedo();
}
/**
 * @param {MyDrawingView} myDrawingView 
 */
function handleButtonCanvasExportSvg(myDrawingView) {
  myDrawingView.handleCanvasExportSvg();
}
/**
 * @param {MyDrawingView} myDrawingView 
 */
function handleButtonCanvasImportSvg(myDrawingView) {
  myDrawingView.handleCanvasImportSvg();
}


const ActionsAvailability = {
  ALWAYS: [
    Actions.CANVAS_RESET,
    Actions.CANVAS_IMPORT_SVG,
  ],
  IMMERSIONS_EXIST: [
    Actions.CANVAS_EXPORT_SVG,
  ],
  IMMERSION_SELECTED: [
    Actions.IMMERSION_DELETE,
    Actions.IMMERSION_DUPLICATE,
    Actions.IMMERSION_SMOOTH,
    Actions.IMMERSION_FLATTEN,
    Actions.IMMERSION_SIMPLIFY,
    Actions.IMMERSION_BRING_FORWARD,
    Actions.IMMERSION_SEND_BACK,
    Actions.IMMERSION_UNDO,
    Actions.IMMERSION_REDO,
  ],
}

export { Actions, ActionsAvailability };
