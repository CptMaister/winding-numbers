import MyImmersion from "../classes/myImmersion.js";
import { sortNumbersAscending } from "../helpers/basic.js";

const ImmersionInfo = Object.freeze({
  CLOSED: {
    symbol: Symbol('closed'),
    handler: handleClosed,
  },
  TANGENTIAL_INTERSECTIONS: {
    symbol: Symbol('tangential-intersections'),
    handler: handleTangentialIntersections,
  },
  CROSSINGS: {
    symbol: Symbol('crossings'),
    handler: handleCrossings,
  },
  PARTS: {
    symbol: Symbol('parts'),
    handler: handleParts,
  },
  CONNECTED_COMPONENTS: {
    symbol: Symbol('connected-components'),
    handler: handleConnectedComponents,
  },
  WINDING_NUMBERS: {
    symbol: Symbol('winding-numbers'),
    handler: handleWindingNumbers,
  },
  DOUBLE_POINT_INDICES: {
    symbol: Symbol('dp-indices'),
    handler: handleDoublePointIndices,
  },
  J_PLUS: {
    symbol: Symbol('jplus'),
    handler: handleJPlus,
  },
  ROTATION_NUMBER: {
    symbol: Symbol('rotation-number'),
    handler: handleRotationNumber,
  },
});

/**
 * @param {MyImmersion} myImmersion
 * @returns {String}
 */
function handleClosed(myImmersion) {
  return myImmersion.closed ? "true" : "false";
}
/**
 * @param {MyImmersion} myImmersion
 * @returns {String}
 */
function handleTangentialIntersections(myImmersion) {
  return myImmersion.noTangentialCrossings ? "false" : "true";
}
/**
 * @param {MyImmersion} myImmersion
 * @returns {String}
 */
function handleCrossings(myImmersion) {
  return myImmersion.selfIntersectionsCount.toString();
}
/**
 * @param {MyImmersion} myImmersion
 * @returns {String}
 */
function handleParts(myImmersion) {
  return myImmersion.immersionPartsCount.toString();
}
/**
 * @param {MyImmersion} myImmersion
 * @returns {String}
 */
function handleConnectedComponents(myImmersion) {
  return myImmersion.connectedComponentsCount.toString();
}
/**
 * @param {MyImmersion} myImmersion
 * @returns {String}
 */
function handleWindingNumbers(myImmersion) {
  return myImmersion.connectedComponentWindingNumbers
    .sort(sortNumbersAscending)
    .join(", ");
}
/**
 * @param {MyImmersion} myImmersion
 * @returns {String}
 */
function handleDoublePointIndices(myImmersion) {
  return myImmersion.doublePointIndices
    .sort(sortNumbersAscending)
    .join(", ");
}
/**
 * @param {MyImmersion} myImmersion
 * @returns {String}
 */
function handleJPlus(myImmersion) {
  return myImmersion.jPlus.toString();
}
/**
 * @param {MyImmersion} myImmersion
 * @returns {String}
 */
function handleRotationNumber(myImmersion) {
  return myImmersion.rotationNumber.toString();
}

export { ImmersionInfo };
