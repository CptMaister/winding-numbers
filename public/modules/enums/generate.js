
const Generate = Object.freeze({
  NOTHING: Symbol('nothing'),
  BIFURCATION: Symbol('bifurcation'),
  ROTATED_KEPLER_ELLIPSE: Symbol('rotated-kepler-ellipse'),
  LEVI_CIVITA_REGULARIZATION: Symbol('levi-civita-regularization'),
  BIRKHOFF_REGULARIZATION: Symbol('birkhoff-regularization'),
});

const GenerateAvailability = {
  ALWAYS: [
    Generate.NOTHING,
    Generate.ROTATED_KEPLER_ELLIPSE,
  ],
  IMMERSIONS_EXIST: [],
  IMMERSION_SELECTED: [
    Generate.BIFURCATION,
    Generate.LEVI_CIVITA_REGULARIZATION,
    Generate.BIRKHOFF_REGULARIZATION,
  ],
}

export { Generate, GenerateAvailability };
