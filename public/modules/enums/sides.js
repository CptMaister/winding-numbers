
const Sides = Object.freeze({
  LEFT: Symbol('left'),
  RIGHT: Symbol('right'),
});
function isSide(side) {
  return [Sides.LEFT, Sides.RIGHT].includes(side);
}
function flipSide(side, flip) {
  if (flip) {
    if (side === Sides.RIGHT) {
      return Sides.LEFT;
    } else if (side === Sides.LEFT) {
      return Sides.RIGHT;
    }
  }
  return side;
}

export { Sides, isSide, flipSide };
