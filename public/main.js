import MySettings from "./modules/classes/mySettings.js";
import MyGUI from "./modules/classes/myGui.js";
import MyDrawingView from "./modules/classes/myDrawingView.js";

import { MouseMode, MouseModesAvailability } from "./modules/enums/mouseMode.js";
import { Actions, ActionsAvailability } from "./modules/enums/actions.js";
import runTests from "./modules/test/main.js";
import { CANVAS_ID } from "./modules/constants/config.js";
import { DEBUGGING_TESTS_RUN_ALL } from "./modules/constants/debug.js";



///*** Enums ***///

// this is some leftover from restructuring (when this project changed from one js-file to multiple files, to add an npm package), but will be deleted anyway before production // TODO ALEX END delete

function compareUIEnumAndItsAvailability(uiEnum, availability) {
  if (Object.values(uiEnum).length !== Object.values(availability).flat().length) {
    console.error('compareUIEnumAndItsAvailability');
    console.warn({uiEnum});
    console.warn({availability});
  }
}
compareUIEnumAndItsAvailability(MouseMode, MouseModesAvailability);
compareUIEnumAndItsAvailability(Actions, ActionsAvailability);





/////////////////////////////////////////////////////
///-----------------------------------------------///
///                                               ///
///  ~~~  Here we actually initialize stuff  ~~~  ///
///                                               ///
///-----------------------------------------------///
/////////////////////////////////////////////////////

///*** Initialization ***///

window.onload = onLoadSetup;

// TODO ALEX CONTINUE create new class MySession? replace myOgStuff
function onLoadSetup() {
  const mySettings = new MySettings();
  const myGUI = new MyGUI(mySettings.DEFAULT_SETTINGS.mouseMode);
  const myDrawingView = new MyDrawingView(CANVAS_ID, myGUI, mySettings);
  
  runTests(DEBUGGING_TESTS_RUN_ALL);
}



//////////////////////////////////////////////////
///--------------------------------------------///
///                                            ///
///  ~~~  Licenses and Copyright Notices  ~~~  ///
///                                            ///
///--------------------------------------------///
//////////////////////////////////////////////////

/**
http://paperjs.org/license/

Paper.js is distributed under the permissive MIT License:

Copyright (c) 2011, Juerg Lehni & Jonathan Puckey
http://lehni.org/ & http://jonathanpuckey.com/
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

https://github.com/glenzli/paperjs-offset/blob/master/LICENSE

MIT License

Copyright (c) 2016-2019 luz-alphacode

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

And this is the License of this software :-)
https://gitlab.com/CptMaister/winding-numbers/-/blob/main/LICENSE?ref_type=heads

MIT License

Copyright (c) 2024 Alexander Mai

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/




//////////////////////////////////////////////////
///--------------------------------------------///
///                                            ///
///  ~~~  Some kind of Roadmap and stuff  ~~~  ///
///                                            ///
///--------------------------------------------///
//////////////////////////////////////////////////

// Beim platzieren von Punkten (Origin, Target, E, M) wird bei Klick platziert, aber wenn man länger als zB 1s den Klick hält, wird es wieder gepackt und erst bei Release gesetzt

// Shortcuts: for most mouse modes and actions!

// Mouse handlers:
// - scroll mouse -> zooms view
// - Drag Mode:
//   - during click, scroll mouse -> scales selected immersion
//   - double click: select immersion
//   - double click on selected immersion: Handles Mode
//   - double click on origin, E or M: respective Mode


// Settings:

// Preview Generate
// - show preview: true (also shows preview during immersion drawing, if any generate is selected)
//   - at position: [target, origin, between]

// Unit circle
// - show around origin: false
//   - size: slider
// - show around target: false
//   - size: slider

// Origin, E, M:
// - adjust automatically: [origin, E or M]
// - snap at (checkboxes:) horizontal, vertical

// Auto run and performance
// - checkbox true "always calculate self-intersections" (whenever a new immersion is drawn or finished editing. Toggling this on will not do this for uncalculated immersions)
//   - button "calculate self-intersections" (disabled if checkbox was never retoggled to true; only applies to selected component)

// - checkbox true "always calculate connected components"
//   - button "calculate connected components"  (this also calculates the dpIndices, jPlus and rotationNumber, since they are almost free after CCs)

// - checkbox true "calculate self-intersections during editing"
//   - slider "self-intersections edit calculation intervalls" 1/30s (1/30s bis 3s, evtl logarithmisch, oder mit groves)

// - checkbox false "calculate connected components during editing"
//   - slider "cc edit calculation intervalls" 1/5s (1/30s bis 3s, evtl logarithmisch, oder mit groves)

// Import / Export settings


// Export and Import of curve data, maybe as flattened curve points, or idk


// TODO ALEX CONTINUE
// make immersion-visuals-settings belong to an immersion or to the UI-session

// visuals: bifurcation path-tube-thingy

// BUGS:
// - ...

// END:
// - add keyboard shortcuts
